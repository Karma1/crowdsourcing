Visenze ViFind System
======

## Setup Guide

1. Download the source code
2. Create images folder in vifind-core project webapp resource folder
3. Import all projects into eclipse
4. Copy all `config.properties.sample` as `config.properties` and change settings accordingly
5. Add `jar` file in `libs` folder into project build path
6. Start server in order: vifind-socket -> vifind-core -> vifind-client

## Project Structure

        ├── README.md
        ├── script
        │   └── test.sh
        ├── socketserver
        │   └── vifind-socket
        │       ├── pom.xml
        │       ├── src
        │       │   ├── main
        │       │   │   ├── java
        │       │   │   │   └── com
        │       │   │   │       └── visenze
        │       │   │   │           └── vifind
        │       │   │   │               └── socket
        │       │   │   │                   ├── Command.java
        │       │   │   │                   └── TaskDistributeServer.java
        │       │   │   └── resources
        │       │   │       ├── config.properties
        │       │   │       └── config.properties.sample
        │       │   └── test
        │       │       └── java
        │       │           └── com
        │       │               └── visenze
        │       │                   └── vifind
        │       │                       └── socket
        │       │                           └── AppTest.java
        │       └── target
        │           ├── classes
        │           │   ├── com
        │           │   │   └── visenze
        │           │   │       └── vifind
        │           │   │           └── socket
        │           │   │               ├── Command.class
        │           │   │               ├── TaskDistributeServer$1.class
        │           │   │               ├── TaskDistributeServer$2.class
        │           │   │               ├── TaskDistributeServer$3.class
        │           │   │               ├── TaskDistributeServer$4.class
        │           │   │               └── TaskDistributeServer.class
        │           │   ├── config.properties
        │           │   ├── config.properties.sample
        │           │   └── META-INF
        │           │       ├── MANIFEST.MF
        │           │       └── maven
        │           │           └── com.visenze.vifind
        │           │               └── vifind-socket
        │           │                   ├── pom.properties
        │           │                   └── pom.xml
        │           └── test-classes
        │               └── com
        │                   └── visenze
        │                       └── vifind
        │                           └── socket
        │                               └── AppTest.class
        ├── temp
        └── webserver
        ├── vifind-client
        │   ├── log.txt
        │   ├── pom.xml
        │   └── src
        │       ├── main
        │       │   ├── java
        │       │   │   └── com
        │       │   │       └── visenze
        │       │   │           └── vifind
        │       │   │               ├── config
        │       │   │               │   └── Config.java
        │       │   │               ├── controller
        │       │   │               │   └── RecognitionController.java
        │       │   │               ├── service
        │       │   │               │   └── RecognitionService.java
        │       │   │               └── util
        │       │   │                   ├── APIHelper.java
        │       │   │                   └── LogHelper.java
        │       │   ├── resources
        │       │   │   ├── log4j.xml
        │       │   │   └── META-INF
        │       │   └── webapp
        │       │       ├── resources
        │       │       └── WEB-INF
        │       │           ├── classes
        │       │           ├── spring
        │       │           │   ├── appServlet
        │       │           │   │   └── servlet-context.xml
        │       │           │   └── root-context.xml
        │       │           ├── views
        │       │           │   └── home.jsp
        │       │           └── web.xml
        │       └── test
        │           ├── java
        │           │   └── com
        │           │       └── visenze
        │           │           └── vifind
        │           └── resources
        │               └── log4j.xml
        └── vifind-core
            ├── libs
            │   └── socketio.jar
            ├── pom.xml
            └── src
                ├── main
                │   ├── java
                │   │   └── com
                │   │       └── visenze
                │   │           └── vifind
                │   │               ├── config
                │   │               │   ├── Category.java
                │   │               │   ├── Config.java
                │   │               │   └── Variable.java
                │   │               ├── controller
                │   │               │   ├── SessionController.java
                │   │               │   ├── TaskController.java
                │   │               │   └── WorkerController.java
                │   │               ├── dto
                │   │               │   ├── BaseResponse.java
                │   │               │   ├── CreateResponse.java
                │   │               │   ├── GetResponse.java
                │   │               │   ├── StatusResponse.java
                │   │               │   ├── UpdateResponse.java
                │   │               │   └── WorkerResponse.java
                │   │               ├── model
                │   │               │   ├── STATUS.java
                │   │               │   ├── Subtask.java
                │   │               │   ├── Task.java
                │   │               │   ├── TYPE.java
                │   │               │   └── Worker.java
                │   │               ├── service
                │   │               │   ├── TaskService.java
                │   │               │   └── WorkerService.java
                │   │               ├── strategy
                │   │               │   ├── DefaultStrategy.java
                │   │               │   ├── StrategyFactory.java
                │   │               │   └── Strategy.java
                │   │               ├── template
                │   │               │   ├── CheckboxTemplate.java
                │   │               │   ├── EmptyTemplate.java
                │   │               │   ├── ReviewTemplate.java
                │   │               │   ├── TemplateAbstract.java
                │   │               │   └── TypeinTemplate.java
                │   │               └── util
                │   │                   ├── ApplicationContextHolder.java
                │   │                   ├── PropertyHelper.java
                │   │                   └── SocketIOHelper.java
                │   ├── resources
                │   │   ├── category
                │   │   ├── config.properties
                │   │   ├── config.properties.sample
                │   │   ├── log4j.xml
                │   │   └── META-INF
                │   └── webapp
                │       ├── resources
                │       │   ├── css
                │       │   │   ├── bootstrap.css
                │       │   │   ├── bootstrap.min.css
                │       │   │   ├── bootstrap-theme.css
                │       │   │   ├── bootstrap-theme.min.css
                │       │   │   ├── login.css
                │       │   │   └── main.css
                │       │   ├── fonts
                │       │   │   ├── glyphicons-halflings-regular.eot
                │       │   │   ├── glyphicons-halflings-regular.svg
                │       │   │   ├── glyphicons-halflings-regular.ttf
                │       │   │   └── glyphicons-halflings-regular.woff
                │       │   ├── images
                │       │   └── js
                │       │       ├── bootstrap.js
                │       │       ├── bootstrap.min.js
                │       │       ├── jquery-1.10.2.js
                │       │       ├── main.js
                │       │       ├── moment.js
                │       │       └── socket.io
                │       │           ├── socket.io.js
                │       │           ├── WebSocketMainInsecure.swf
                │       │           └── WebSocketMain.swf
                │       └── WEB-INF
                │           ├── classes
                │           ├── spring
                │           │   ├── appServlet
                │           │   │   └── servlet-context.xml
                │           │   ├── mongo.xml
                │           │   ├── root-context.xml
                │           │   └── security-context.xml
                │           ├── views
                │           │   ├── 500.html
                │           │   ├── login.jsp
                │           │   ├── register.jsp
                │           │   └── workers.jsp
                │           └── web.xml
                └── test
                    ├── java
                    │   └── com
                    │       └── visenze
                    │           └── vifind
                    └── resources
                        └── log4j.xml

95 directories, 98 files

