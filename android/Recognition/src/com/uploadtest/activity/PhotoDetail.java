package com.uploadtest.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.uploadtest.R;
import com.uploadtest.common.Constant;
import com.uploadtest.util.ImageDownLoadAsyncTask;
import com.uploadtest.vo.PhotoVO;

public class PhotoDetail extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photo_detail);

		Bundle bundle = this.getIntent().getExtras();
		PhotoVO photo = (PhotoVO) bundle
				.getSerializable(Constant.PHOTO_DETAIL_KEY_STRING);

		TextView textView = (TextView) this.findViewById(R.id.photo_id);
		textView.setText(photo.getId());

		ImageView imageView = (ImageView) this.findViewById(R.id.photo_detail);
		int item_width = getWindowManager().getDefaultDisplay().getWidth();
		ImageDownLoadAsyncTask asyncTask = new ImageDownLoadAsyncTask(this,
				photo.getPhotoURL(), imageView, item_width, 1);
		asyncTask.execute();

	}
}
