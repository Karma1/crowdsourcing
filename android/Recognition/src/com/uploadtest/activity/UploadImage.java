package com.uploadtest.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.uploadtest.R;
import com.uploadtest.bean.RecognizeResult;
import com.uploadtest.common.Constant;
import com.uploadtest.common.VisenzeApplication;
import com.uploadtest.util.AlbumStorageDirFactory;
import com.uploadtest.util.BaseAlbumDirFactory;
import com.uploadtest.util.FroyoAlbumDirFactory;
import com.uploadtest.util.ImageUtil;

public class UploadImage extends Activity {

	static final int SELECT_PHOTO = 0;
	static final int TAKE_PHOTO = 1;

	private static final String FILE_PATH = "FILEURL";

	private Button selectBtn;
	private Button takePhotoBtn;
	private ProgressDialog m_Dialog;

	private String mCurrentPhotoPath;
	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
		addListenerOnButton();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
	}

	@SuppressLint("NewApi")
	private void init() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		((VisenzeApplication) getApplicationContext()).setScreenWidth(size.x);
		((VisenzeApplication) getApplicationContext()).setScreenHeight(size.y);
	}

	@Override
	protected void onResume() {
		super.onResume();
		addControl();
	}

	private void addListenerOnButton() {
		selectBtn = (Button) findViewById(R.id.select_btn);
		takePhotoBtn = (Button) findViewById(R.id.take_photo_btn);

		selectBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				removeControl();
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, SELECT_PHOTO);
			}
		});

		takePhotoBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				String state = Environment.getExternalStorageState();
				if (state.equals(Environment.MEDIA_MOUNTED)) {
					removeControl();
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					File f = null;
					try {
						f = setUpPhotoFile();
						mCurrentPhotoPath = f.getAbsolutePath();
						intent.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(f));
						intent.putExtra(FILE_PATH, mCurrentPhotoPath);
					} catch (IOException e) {
						e.printStackTrace();
						f = null;
						mCurrentPhotoPath = null;
					}
					startActivityForResult(intent, TAKE_PHOTO);
				} else {
					Toast.makeText(UploadImage.this, "please insert SD card",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PHOTO) {
				Uri uri = data.getData();
				try {
					String[] pojo = { MediaStore.Images.Media.DATA };
					Cursor cursor = getContentResolver().query(uri, pojo, null,
							null, null);
					if (cursor != null) {
						int colunm_index = cursor
								.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
						cursor.moveToFirst();
						mCurrentPhotoPath = cursor.getString(colunm_index);
					}
				} catch (Exception e) {

				}
			} else if (requestCode == TAKE_PHOTO) {
				if (mCurrentPhotoPath == null) {
					mCurrentPhotoPath = data.getStringExtra(FILE_PATH);
				}
				Log.d("gg", String.valueOf(mCurrentPhotoPath == null));
				galleryAddPic();
			}
			new UploadTask().execute();
		}
	}

	private class UploadTask extends AsyncTask<Void, Void, String> {
		protected String doInBackground(Void... arg) {

			String jsonString = null;
			Bitmap bitmap = ImageUtil
					.getResizedRotatedBitmapForServer(mCurrentPhotoPath);

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			byte[] byte_arr = stream.toByteArray();

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			entity.addPart("photo", new ByteArrayBody(byte_arr, ""));
			try {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(Constant.APP_SITE_ADDRESS);
				httppost.setEntity(entity);
				HttpResponse response = httpclient.execute(httppost);
				jsonString = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonString;
		}

		protected void onPostExecute(String jsonString) {
			m_Dialog.dismiss();
			if (jsonString != null) {
				RecognizeResult result = new Gson().fromJson(jsonString,
						RecognizeResult.class);
				if (result.getSearchJson().equals("empty")) {
					new AlertDialog.Builder(UploadImage.this)
							.setTitle("Only recognize result")
							.setMessage(result.getRecognizeString())
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.cancel();
										}
									}).show();
				} else {
					Bundle bundle = new Bundle();
					bundle.putString(Constant.UPLOAD_ONE_STRING,
							result.getRecognizeString());
					bundle.putString(Constant.UPLOAD_TWO_STRING,
							result.getSearchJson());
					Intent uploadListIntent = new Intent(UploadImage.this,
							UploadList.class);
					uploadListIntent.putExtras(bundle);
					startActivity(uploadListIntent);
				}
			} else {
				Toast.makeText(UploadImage.this,
						"Result is null, please check network! ",
						Toast.LENGTH_LONG).show();
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			m_Dialog = ProgressDialog.show(UploadImage.this, "Please Wait ...",
					"Uploading ...", true);
		}

	}

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = Constant.JPEG_FILE_PREFIX + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName,
				Constant.JPEG_FILE_SUFFIX, albumF);
		return imageF;
	}

	private File setUpPhotoFile() throws IOException {
		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();
		return f;
	}

	private String getAlbumName() {
		return getString(R.string.album_name);
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			storageDir = mAlbumStorageDirFactory
					.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d("camera", "failed to create directory");
						return null;
					}
				}
			}
		} else {
			Log.v(getString(R.string.app_name),
					"External storage is not mounted READ/WRITE.");
		}
		return storageDir;
	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	private void addControl() {
		selectBtn.setEnabled(true);
		takePhotoBtn.setEnabled(true);
	}

	private void removeControl() {
		selectBtn.setEnabled(false);
		takePhotoBtn.setEnabled(false);
	}
}
