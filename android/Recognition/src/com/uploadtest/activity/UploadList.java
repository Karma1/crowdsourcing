package com.uploadtest.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.uploadtest.R;
import com.uploadtest.bean.Photo;
import com.uploadtest.bean.UploadResult;
import com.uploadtest.common.Constant;
import com.uploadtest.common.Converter;
import com.uploadtest.common.VisenzeApplication;
import com.uploadtest.component.LazyScrollView;
import com.uploadtest.util.ImageDownLoadAsyncTask;
import com.uploadtest.vo.PhotoVO;

public class UploadList extends Activity implements
		LazyScrollView.OnScrollListener {
	private LazyScrollView lazyScrollView;
	private LinearLayout waterfall_container;
	private ArrayList<LinearLayout> linearLayouts;

	private LinearLayout progressbar;

	private TextView loadtext;

	private List<Photo> photoList;
	private ImageDownLoadAsyncTask asyncTask;

	private int current_page = 0;
	private int count = 20;// show numbers per page

	private int item_width;// waterfall column width

	/***
	 * init view
	 */
	public void initView() {
		setContentView(R.layout.list);
		lazyScrollView = (LazyScrollView) findViewById(R.id.waterfall_scroll);
		lazyScrollView.getView();
		lazyScrollView.setOnScrollListener(this);
		waterfall_container = (LinearLayout) findViewById(R.id.waterfall_container);
		progressbar = (LinearLayout) findViewById(R.id.progressbar);
		loadtext = (TextView) findViewById(R.id.loadtext);

		item_width = ((VisenzeApplication) getApplicationContext())
				.getScreenWidth() / Constant.WATERFALL_COLUMN;
		// item_width = getWindowManager().getDefaultDisplay().getWidth() /
		// Constant.WATERFALL_COLUMN;
		linearLayouts = new ArrayList<LinearLayout>();

		// add columns to waterfall_container
		for (int i = 0; i < Constant.WATERFALL_COLUMN; i++) {
			LinearLayout layout = new LinearLayout(this);
			LinearLayout.LayoutParams itemParam = new LinearLayout.LayoutParams(
					item_width, LayoutParams.WRAP_CONTENT);
			layout.setOrientation(LinearLayout.VERTICAL);
			layout.setLayoutParams(itemParam);
			layout.setPadding(5, 5, 5, 5);
			linearLayouts.add(layout);
			waterfall_container.addView(layout);
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String recognizeResult = this.getIntent().getExtras()
				.getString(Constant.UPLOAD_ONE_STRING);
		getActionBar().setTitle(recognizeResult);
		String json = null;
		try {
			json = this.getIntent().getExtras()
					.getString(Constant.UPLOAD_TWO_STRING);
			UploadResult jsonObj = new Gson()
					.fromJson(json, UploadResult.class);
			photoList = jsonObj.getResult();
			if (photoList.isEmpty()) {
				Toast.makeText(UploadList.this, "NO RESULT!", Toast.LENGTH_LONG)
						.show();
				return;
			} else {
				initView();
				// load first time
				addImage(current_page, count);
			}
		} catch (Exception e) {
			Log.i("gg", json);
			Log.e("gg", e.getMessage());
			Toast.makeText(UploadList.this, "JSON FORMAT ERROR!",
					Toast.LENGTH_LONG).show();
			this.finish();
			return;
		}
	}

	/***
	 * load more
	 * 
	 * @param current_page
	 * @param count
	 */
	private void addImage(int current_page, int count) {
		int j = 0;
		int imagecount = photoList.size();
		for (int i = current_page * count; i < count * (current_page + 1)
				&& i < imagecount; i++) {
			Map map = photoList.get(i).getValue_map();
			addBitMapToImage((String) map.get("im_s3_url"), j, i);
			j++;
			if (j >= Constant.WATERFALL_COLUMN)
				j = 0;
		}

	}

	/***
	 * add image to imageview
	 * 
	 * @param string
	 *            image url
	 * @param j
	 *            column index
	 * @param i
	 *            images index
	 */
	private void addBitMapToImage(String imageUrl, int j, int i) {
		ImageView imageView = new ImageView(this);
		asyncTask = new ImageDownLoadAsyncTask(this, imageUrl, imageView,
				item_width, 5);

		asyncTask.setProgressbar(progressbar);
		asyncTask.setLoadtext(loadtext);
		asyncTask.execute();

		imageView.setTag(i);
		linearLayouts.get(j).addView(imageView);

		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showPhotoDetail(Converter.converte2PhotoVO(photoList
						.get((Integer) v.getTag())));
			}
		});
	}

	private void showPhotoDetail(PhotoVO photo) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(Constant.PHOTO_DETAIL_KEY_STRING, photo);
		Intent photoDetailIntent = new Intent(this, PhotoDetail.class);
		photoDetailIntent.putExtras(bundle);
		startActivity(photoDetailIntent);
	}

	@Override
	public void onBottom() {
		// addImage(++current_page, count);

	}

	@Override
	public void onTop() {

	}

	@Override
	public void onScroll() {

	}

}