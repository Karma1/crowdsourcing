package com.uploadtest.bean;

import java.util.Map;

public class Photo {
	private String im_name;
	private Map value_map;
	public String getIm_name() {
		return im_name;
	}
	public void setIm_name(String im_name) {
		this.im_name = im_name;
	}
	public Map getValue_map() {
		return value_map;
	}
	public void setValue_map(Map value_map) {
		this.value_map = value_map;
	}
}
