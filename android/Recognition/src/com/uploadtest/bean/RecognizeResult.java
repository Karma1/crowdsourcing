package com.uploadtest.bean;

public class RecognizeResult {
	private String status;
	private String searchJson;
	private String recognizeString;

	public RecognizeResult(String status, String searchJson,
			String recognizeString) {
		super();
		this.status = status;
		this.searchJson = searchJson;
		this.recognizeString = recognizeString;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSearchJson() {
		return searchJson;
	}

	public void setSearchJson(String searchJson) {
		this.searchJson = searchJson;
	}

	public String getRecognizeString() {
		return recognizeString;
	}

	public void setRecognizeString(String recognizeString) {
		this.recognizeString = recognizeString;
	}

	@Override
	public String toString() {
		return "RecognizeResult [status=" + status + ", searchJson="
				+ searchJson + ", recognizeString=" + recognizeString + "]";
	}

}
