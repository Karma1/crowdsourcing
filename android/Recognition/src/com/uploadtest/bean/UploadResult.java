package com.uploadtest.bean;

import java.util.List;

public class UploadResult {

	private String status;
	private String method;
	private List<String> error;
	private int total;
	private int page;
	private int limit;
	private List<Photo> result;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public List<String> getError() {
		return error;
	}
	public void setError(List<String> error) {
		this.error = error;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public List<Photo> getResult() {
		return result;
	}
	public void setResult(List<Photo> result) {
		this.result = result;
	}
	
	
}
