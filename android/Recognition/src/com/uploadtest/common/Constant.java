package com.uploadtest.common;

public class Constant {

	public static String APP_SITE_ADDRESS = "http://172.26.191.184:8081/vifind-client/demo";

	public static final String JPEG_FILE_PREFIX = "IMG_";
	public static final String JPEG_FILE_SUFFIX = ".jpg";

	public static final String UPLOAD_ONE_STRING = "recognizeresult";
	public static final String UPLOAD_TWO_STRING = "searchjson";
	public static final String PHOTO_DETAIL_KEY_STRING = "photoDetail";

	public static final String PHOTO_BITMAP_KEY_STRING = "bitmapImageUri";

	// waterfall columns
	public static final int WATERFALL_COLUMN = 2;
	public static final float UPLOAD_IMAGE_LONG_SIZE = 640f;
}
