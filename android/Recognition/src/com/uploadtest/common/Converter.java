package com.uploadtest.common;

import java.util.Map;

import com.uploadtest.bean.Photo;
import com.uploadtest.vo.PhotoVO;

public class Converter {
	public static PhotoVO converte2PhotoVO(Photo photo) {
		PhotoVO photoVo = new PhotoVO();
		Map value_map = photo.getValue_map();
		photoVo.setId((String) value_map.get("_id"));
		photoVo.setPhotoURL((String) value_map.get("im_s3_url"));
		return photoVo;
	}
}
