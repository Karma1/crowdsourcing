package com.uploadtest.common;

public class Response {

	private String result;
	private String token;
	private String error;
	private String type;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String toString() {
		return "error: " + error + " result: " + result + " token: " + token
				+ " type: " + type;
	}

}
