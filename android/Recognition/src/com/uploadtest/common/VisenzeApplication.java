package com.uploadtest.common;

import android.app.Application;
import android.util.Log;

public class VisenzeApplication extends Application {

	private int screenWidth;
	private int screenHeight;

	public void onCreate() {
		Log.v("gg", "VisenzeApplication on create");
	}

	public VisenzeApplication() {
		super();
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}

	public int getScreenHeight() {
		return screenHeight;
	}

	public void setScreenHeight(int screenHeight) {
		this.screenHeight = screenHeight;
	}

}
