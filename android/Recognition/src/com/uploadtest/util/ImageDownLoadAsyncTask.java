package com.uploadtest.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ImageDownLoadAsyncTask extends AsyncTask<Void, Void, Bitmap> {
	private String imagePath;
	private ImageView imageView;
	private int image_width;// image show width
	private LinearLayout progressbar;
	private TextView loadtext;
	private int tmpScale;

	/**
	 * 构造方法
	 * 
	 * @param context
	 * @param imagePath
	 * @param imageView
	 */
	public ImageDownLoadAsyncTask(Context context, String imagePath,
			ImageView imageView, int image_width, int tmpScale) {
		this.imagePath = imagePath;
		this.imageView = imageView;
		this.image_width = image_width;
		this.tmpScale = tmpScale;
	}

	public void setLoadtext(TextView loadtext) {
		this.loadtext = loadtext;
	}

	public void setProgressbar(LinearLayout progressbar) {
		this.progressbar = progressbar;
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		return NetworkUtil.getBitmapFromServer(imagePath, image_width, 0,
				tmpScale);
	}

	@Override
	protected void onPostExecute(Bitmap drawable) {
		super.onPostExecute(drawable);

		if (drawable != null) {
			LayoutParams layoutParams = imageView.getLayoutParams();
			int height = drawable.getHeight();
			int width = drawable.getWidth();
			layoutParams.height = (height * image_width) / width;

			imageView.setLayoutParams(layoutParams);
			imageView.setImageBitmap(drawable);
		}
		if (progressbar != null
				&& (progressbar.isShown() || loadtext.isShown())) {
			progressbar.setVisibility(View.GONE);
			loadtext.setVisibility(View.GONE);
		}

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (loadtext != null && !loadtext.isShown()) {
			loadtext.setVisibility(View.VISIBLE);
		}

	}
}