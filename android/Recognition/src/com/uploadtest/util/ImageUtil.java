package com.uploadtest.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import com.uploadtest.common.Constant;

public class ImageUtil {

	/**
	 * the return image's longer side will be 1500px. rotate is to fit for
	 * samsung camera taking picture auto rotate problem
	 * 
	 * @param originalBitmap
	 * @return resizeBitmap
	 */
	public static Bitmap getResizedRotatedBitmapForServer(String originalPath) {

		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(originalPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;
		int longerSide = (photoW > photoH) ? photoW : photoH;

		int scaleFactor = 1;
		if (longerSide > Constant.UPLOAD_IMAGE_LONG_SIZE) {
			scaleFactor = (int) Math.floor(longerSide
					/ Constant.UPLOAD_IMAGE_LONG_SIZE);
		}

		bmOptions.inJustDecodeBounds = false;
		bmOptions.inPurgeable = true;
		bmOptions.inSampleSize = scaleFactor;

		/* get bitmap by file path */
		Bitmap tmpBitmap = BitmapFactory.decodeFile(originalPath, bmOptions);
		Log.v("gg", "original path:" + originalPath);

		/* Then compress exactly */
		int bitmapWidth = tmpBitmap.getWidth();
		int bitmapHeight = tmpBitmap.getHeight();

		Matrix matrix = new Matrix();
		matrix.postRotate(getImageDegree(originalPath));
		Bitmap bitmap = Bitmap.createBitmap(tmpBitmap, 0, 0, bitmapWidth,
				bitmapHeight, matrix, true);
		return bitmap;
	}

	public static Bitmap getCompressBitmap(InputStream in, int width,
			int height, int tmpScale) throws IOException {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageUtil.copy(in, out);
		InputStream in2 = new ByteArrayInputStream(out.toByteArray());

		BitmapFactory.decodeStream(in, null, options);

		options.inSampleSize = tmpScale;
		// options.inSampleSize = ImageUtil.calculateInSampleSize(options,
		// width,height);
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeStream(in2, null, options);
		return bitmap;
	}

	public static int copy(InputStream input, OutputStream output)
			throws IOException {
		byte[] buffer = new byte[1024];
		int count = 0;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}

	public static int getImageDegree(String path) {
		int degree = 0;
		try {
			ExifInterface exifInterface = new ExifInterface(path);
			int orientation = exifInterface.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return degree;
	}
}
