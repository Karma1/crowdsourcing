package com.uploadtest.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.util.Log;

public class NetworkUtil {
	public static String getStringFromServer(String url) {
		try {
			HttpGet httpRequest = new HttpGet(url);
			HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode != 200) {
				Log.v("Error:", "Error " + statusCode + url);
				return null;
			}

			HttpEntity entity = httpResponse.getEntity();
			if (entity != null) {
				InputStream is = entity.getContent();
				StringBuffer sb = new StringBuffer();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";

				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
				String result = sb.toString();
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Error:", "Error while retrieving bitmap from " + url);
		}
		return null;
	}

	public static Bitmap getBitmapFromServer(String url,int width,int height,int tmpScale) {
		try {
			HttpGet httpRequest = new HttpGet(url);
			HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Log.v("Error:", "Error " + statusCode + " while retrieving bitmap from " + url);
				return null;
			}

			HttpEntity entity = httpResponse.getEntity();
			InputStream inputStream = null;
			if (entity != null) {
				try {
					inputStream = entity.getContent();
					Bitmap bitmap = ImageUtil.getCompressBitmap(inputStream, width,height,tmpScale);
					return bitmap;
				} catch (Exception e) {
					e.printStackTrace();
					Log.v("Error:", "Error while retrieving bitmap from " + url);
				}

				finally {
					if (inputStream != null) {
						inputStream.close();
					}
					entity.consumeContent();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Error:", "Error while retrieving bitmap from " + url);
		}

		return null;
	}

	@SuppressWarnings("finally")
	public static String getStringFromServer(String httpUrl, String format) {
		String result = "";
		try {
			HttpPost request = new HttpPost(httpUrl);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("par", "request-post"));
			HttpEntity entity = new UrlEncodedFormEntity(params, format);
			request.setEntity(entity);
			HttpClient hc = new DefaultHttpClient();
			HttpResponse response = hc.execute(request);
			result = EntityUtils.toString(response.getEntity());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;
		}
	}

	public static String getStringFromServer(String httpUrl, String format, List<NameValuePair> postParams)
			throws IOException {
		HttpPost request = new HttpPost(httpUrl);
		HttpEntity entity = new UrlEncodedFormEntity(postParams, format);
		request.setEntity(entity);
		HttpClient hc = new DefaultHttpClient();
		HttpResponse response = hc.execute(request);
		String result = EntityUtils.toString(response.getEntity());
		return result;
	}

	public static boolean isNetWorkAvailable(Context context) {

		boolean netSataus = false;
		ConnectivityManager cwjManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		cwjManager.getActiveNetworkInfo();

		if (cwjManager.getActiveNetworkInfo() != null) {
			netSataus = cwjManager.getActiveNetworkInfo().isAvailable();
		}
		return netSataus;
	}

}
