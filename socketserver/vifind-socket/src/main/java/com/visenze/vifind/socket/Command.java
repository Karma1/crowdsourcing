package com.visenze.vifind.socket;

/**
 * @author gaoxiang
 * 
 */
public class Command {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Command [message=" + message + "]";
	}

}
