package com.visenze.vifind.socket;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.SortingParams;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIONamespace;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.corundumstudio.socketio.store.RedisStoreFactory;

public class TaskDistributeServer {

	// memory pool
	// private static Queue<String> workerQueue = new ArrayDeque<String>();
	// private static Queue<String> taskQueue = new ArrayDeque<String>();
	/*
	 * Memory Queue interface
	 * 
	 * private static void assignTask(String task) { if (!workerQueue.isEmpty())
	 * { String workerID = workerQueue.poll(); server.getRoomOperations(workerID
	 * + "/output/" + workerID) .sendMessage(task); } else {
	 * taskQueue.offer(task); } logger.info("get new task: " +
	 * workerQueue.size() + " workers now"); }
	 * 
	 * private static void addFreeWorker(String workerID) { if
	 * (!taskQueue.isEmpty()) { server.getRoomOperations(workerID + "/output/" +
	 * workerID) .sendMessage(taskQueue.poll()); } else {
	 * workerQueue.offer(workerID); } logger.info("get new worker: " +
	 * workerQueue.size() + " workers now"); }
	 */

	// logger
	private static final Logger logger = LoggerFactory
			.getLogger(TaskDistributeServer.class);
	// constant names
	private static String WORKER_QUEUE = "worker_queue";
	private static String WORKER_PROCESS = "worker_process";
	private static String WORKER_SET = "worker_set";
	private static String TASK_QUEUE = "task_queue";
	private static String TASK_PROCESS = "task_process";
	private static String DUPLICATE_CLIENT = "duplicate_client";
	private static String ALL_TASK = "all_task";

	// redis client
	private static Jedis redisClient;

	// socketio server
	private static SocketIOServer server;

	// input channel
	private static SocketIONamespace input;

	// output channel
	private static SocketIONamespace output;

	private static void setUpServer() {
		// get configuration
		Properties prop = new Properties();
		try {
			prop.load(TaskDistributeServer.class
					.getResourceAsStream("/config.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Configuration config = new Configuration();

		// setup socketio server
		config.setHostname(prop.getProperty("socketio.host"));
		config.setPort(Integer.valueOf(prop.getProperty("socketio.port")));

		String redisHost = prop.getProperty("redis.host");
		int redisPort = Integer.valueOf(prop.getProperty("redis.port"));
		redisClient = new Jedis(redisHost, redisPort);
		// NOTE: cannot use same instance for redis client and redis subpub
		config.setStoreFactory(new RedisStoreFactory(new Jedis(redisHost,
				redisPort), new Jedis(redisHost, redisPort), new Jedis(
				redisHost, redisPort)));
		server = new SocketIOServer(config);
		redisClient.flushAll();
	}

	private static synchronized void assignTask(String task) {
		logger.info("get new task");
		redisClient.rpoplpush(WORKER_QUEUE, WORKER_PROCESS);
		String workerID = redisClient.rpop(WORKER_PROCESS);
		if (workerID == null) {
			logger.info("task save to queue " + task);
			redisClient.rpush(TASK_QUEUE, task);
			redisClient.sort(TASK_QUEUE, new SortingParams().alpha().asc(),
					TASK_QUEUE);
		} else {
			logger.info("task assign to worker");
			redisClient.set(workerID, task);
			server.getRoomOperations(workerID + "/output/" + workerID)
					.sendMessage(task);
		}
	}

	private static synchronized void addWorker(String workerID) {
		logger.info("get new free worker");
		redisClient.rpoplpush(TASK_QUEUE, TASK_PROCESS);
		String task = redisClient.rpop(TASK_PROCESS);
		if (task == null) {
			logger.info("worker waiting in queue");
			redisClient.lpush(WORKER_QUEUE, workerID);
		} else {
			logger.info("worker process queue task");
			redisClient.set(workerID, task);
			server.getRoomOperations(workerID + "/output/" + workerID)
					.sendMessage(task);
		}
	}

	private static void setUpInputChannel() {
		input = server.addNamespace("/input");
		input.addMessageListener(new DataListener<String>() {
			public void onData(SocketIOClient client, String data,
					AckRequest ackSender) {
				if (redisClient.sismember(ALL_TASK, data)) {
					logger.info("duplicate task received.");
				} else {
					redisClient.sadd(ALL_TASK, data);
					assignTask(data);
				}
			}
		});
	}

	private static void setUpOutputChannel() {
		output = server.addNamespace("/output");

		output.addDisconnectListener(new DisconnectListener() {

			public void onDisconnect(SocketIOClient client) {
				String clientSessionID = client.getSessionId().toString();
				if (redisClient.sismember(DUPLICATE_CLIENT, clientSessionID)) {
					logger.info("remove duplicate client");
					redisClient.srem(DUPLICATE_CLIENT, clientSessionID);
				} else {
					String workerID = client.get("workerID");
					// put unfinished task back in the queue
					String currentTask = redisClient.get(workerID);
					if (currentTask != null) {
						logger.info("unfinished task put back in task queue");
						if (redisClient.llen(TASK_PROCESS) == 0) {
							redisClient.rpush(TASK_PROCESS, currentTask);
						} else {
							redisClient.rpushx(TASK_PROCESS, currentTask);
						}
						redisClient.del(workerID);
						client.del("workerID");
					}
					logger.info("worker log out");
					// remove worker
					if (redisClient.sismember(WORKER_SET, workerID)) {
						redisClient.srem(WORKER_SET, workerID);
					}
					// push next free worker to queue
					redisClient.lrem(WORKER_QUEUE, 0, workerID);
					redisClient.lrem(WORKER_PROCESS, 0, workerID);
					redisClient.rpoplpush(WORKER_QUEUE, WORKER_PROCESS);
				}
			}
		});

		output.addEventListener("registerWorker", Command.class,
				new DataListener<Command>() {

					public void onData(SocketIOClient client, Command c,
							AckRequest ackRequest) {
						String workerID = c.getMessage();
						if (redisClient.sismember(WORKER_SET, workerID)) {
							logger.info("worker  " + workerID
									+ " already registered.");
							redisClient.sadd(DUPLICATE_CLIENT, client
									.getSessionId().toString());
						} else {
							redisClient.sadd(WORKER_SET, workerID);
							client.joinRoom(workerID);
							client.set("workerID", workerID);
							logger.info("worker  " + workerID + " registered");
							addWorker(workerID);
						}
					}
				});

		output.addEventListener("releaseWorker", Command.class,
				new DataListener<Command>() {

					public void onData(SocketIOClient client, Command c,
							AckRequest ackRequest) {
						String workerID = c.getMessage();
						redisClient.del(workerID);
						logger.info("worker  " + workerID + " released");
						addWorker(workerID);
					}
				});
	}

	public static void main(String[] args) throws InterruptedException {

		setUpServer();

		setUpInputChannel();

		setUpOutputChannel();

		server.start();

	}

}
