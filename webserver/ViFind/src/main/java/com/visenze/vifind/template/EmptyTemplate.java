package com.visenze.vifind.template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.Worker;
import com.visenze.vifind.service.WorkerService;

@Component
public class EmptyTemplate extends TemplateAbstract {

	@Autowired
	private WorkerService workerService;

	public EmptyTemplate() {
		super();
	}

	@Override
	public void generateSubtask() {
		if (!discription.equals("N.A.")) {
			task.setTag(task.getTag() + " " + fatherSubtask.getAnswer());
		}

		Subtask rSubtask1 = task.getFinishedTasks().get(
				fatherSubtask.getRelatedTaskTicket());
		Subtask rSubtask2 = task.getFinishedTasks().get(
				rSubtask1.getRelatedTaskTicket());

		Worker rWorker1 = workerService.getWorkerByID(rSubtask1.getWorker());
		Worker rWorker2 = workerService.getWorkerByID(rSubtask2.getWorker());

		if (discription.equals(rSubtask1.getAnswer())) {
			rWorker2.decValidCount();
			workerService.updateWorker(rWorker2);
		} else if (discription.equals(rSubtask2.getAnswer())) {
			rWorker1.decValidCount();
			workerService.updateWorker(rWorker1);
		} else {
			rWorker1.decValidCount();
			rWorker2.decValidCount();
			workerService.updateWorker(rWorker1);
			workerService.updateWorker(rWorker2);
		}
	}

}
