package com.visenze.vifind.template;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.TYPE;

@Component
public class ReviewTemplate extends TemplateAbstract {

	public ReviewTemplate() {
		super();
	}

	@Override
	public void generateSubtask() {
		// TODO Auto-generated method stub
		HashMap<String, Subtask> finishedTasks = task.getFinishedTasks();
		if (finishedTasks.containsKey(fatherSubtask.getRelatedTaskTicket())) {
			Subtask related_subtask = finishedTasks.get(fatherSubtask
					.getRelatedTaskTicket());
			// two related tasks are finished
			if (fatherSubtask.getAnswer().toLowerCase().equals(related_subtask.getAnswer().toLowerCase())) {
				// two answers are equal to each other
				// make it the final answer
				task.setTag(task.getTag() + " " + fatherSubtask.getAnswer().toLowerCase());

			} else {
				// not equal
				// create a review task
				Subtask review_subtask = new Subtask(task.getTicket(),
						task.getImageUrl(), TYPE.review,
						fatherSubtask.getAnswer().toLowerCase() + ";"
								+ related_subtask.getAnswer().toLowerCase());
				review_subtask.setRelatedTaskTicket(fatherSubtask.getTicket());

				task.getWaitingTasks().put(review_subtask.getTicket(),
						review_subtask);
				newSubtasks.add(task.getTicket() + ";"
						+ review_subtask.getTicket());
			}
		}
	}

}
