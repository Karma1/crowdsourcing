package com.visenze.vifind.template;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.visenze.vifind.model.STATUS;
import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;
import com.visenze.vifind.model.Worker;
/**
 * To create a new template, one should implement {@link #generateSubtask}.
 * To get a list of new tasks, one should {@link #processAnswer}, {@link #generateSubtask}, {@link checkStatus}.
 * Call {@link getNewSubtasks} to get the list of new tasks.
 * @author chenc
 *
 */
@Component
public abstract class TemplateAbstract {

	private static final Logger logger = LoggerFactory
			.getLogger(TemplateAbstract.class);

	protected ArrayList<String> newSubtasks;
	protected Task task;
	protected Subtask fatherSubtask;
	protected String discription;

	public TemplateAbstract() {
		newSubtasks = new ArrayList<String>();
	}

	public TemplateAbstract(Task task) {
		newSubtasks = new ArrayList<String>();
		this.task = task;
	}

	abstract public void generateSubtask();

	public void processAnswer(Worker worker, String ticket, Task task,
			TYPE type, String answer, String startTime, String finishedTime) {
		// update subtask
		newSubtasks = new ArrayList<String>();
		HashMap<String, Subtask> waitingTasks = task.getWaitingTasks();
		HashMap<String, Subtask> finishedTasks = task.getFinishedTasks();
		Subtask subtask = waitingTasks.get(ticket);
		subtask.setWorker(worker.getId());
		subtask.setAnswer(answer);
		subtask.setStartTime(startTime);
		subtask.setFinishedTime(finishedTime);
		subtask.setStatus(STATUS.completed);

		// update worker
		try {
			double duration = (double) (new SimpleDateFormat(
					"yyyyMMdd_HHmmssSSS").parse(finishedTime).getTime() - new SimpleDateFormat(
					"yyyyMMdd_HHmmssSSS").parse(startTime).getTime());

			double avgDuration = (worker.getAnswerTime()
					* worker.getTotalCount() + duration)
					/ (double) (worker.getTotalCount() + 1);

			worker.setAnswerTime(avgDuration);
			worker.incTotalCount();
			worker.incValidCount();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// move subtask from waiting list to finished list
		finishedTasks.put(ticket, subtask);
		waitingTasks.remove(ticket);

		// update field parameter
		this.task = task;
		this.discription = answer;
		this.fatherSubtask = subtask;
	}

	public boolean checkStatus() {
		boolean isCompleted = task.getWaitingTasks().isEmpty();
		if (isCompleted) {
			task.setStatus(STATUS.completed);
			task.setFinishedTime(new SimpleDateFormat("yyyyMMdd_HHmmssSSS")
					.format(new Date()));
		}
		try {
			double duration = (double) (new SimpleDateFormat(
					"yyyyMMdd_HHmmssSSS").parse(task.getFinishedTime()).getTime() - new SimpleDateFormat(
					"yyyyMMdd_HHmmssSSS").parse(task.getCreateTime()).getTime());
			logger.info("Task duration is: " + duration + " ms");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isCompleted;
	}

	public ArrayList<String> getNewSubtasks() {
		if (newSubtasks.size() == 0)
			return null;
		return newSubtasks;
	}

	protected void generateRelatedSubtasks(String discription, TYPE type) {
		Subtask subtask1 = new Subtask(task.getTicket(), task.getImageUrl(),
				type, discription);
		Subtask subtask2 = new Subtask(task.getTicket(), task.getImageUrl(),
				type, discription);
		subtask1.setRelatedTaskTicket(subtask2.getTicket());
		subtask2.setRelatedTaskTicket(subtask1.getTicket());

		task.getWaitingTasks().put(subtask1.getTicket(), subtask1);
		task.getWaitingTasks().put(subtask2.getTicket(), subtask2);

		newSubtasks.add(task.getTicket() + ";" + subtask1.getTicket());
		newSubtasks.add(task.getTicket() + ";" + subtask2.getTicket());
	}
}
