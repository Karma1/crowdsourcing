package com.visenze.vifind.template;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.visenze.vifind.config.Category;
import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.TYPE;

@Component
public class TypeinTemplate extends TemplateAbstract {

	public TypeinTemplate() {
		super();
	}

	@Override
	public void generateSubtask() {
		// TODO Auto-generated method stub
		String[] items = discription.split(";");
		HashMap<String, Subtask> finishedTasks = task.getFinishedTasks();
		if (!finishedTasks.containsKey(fatherSubtask.getRelatedTaskTicket())) {
			for (String item : items) { // category
				if (!item.equals("N.A.")) {
					for (String val : Category.categories.get(item)) {

						// for each description of category, we will
						// have #(2) new subtasks.
						// they are each other's related subtask
						String discription = item + ";" + val;
						generateRelatedSubtasks(discription, TYPE.typein);
					}
				}
			}
		} else {
			Subtask related_subtask = finishedTasks.get(fatherSubtask
					.getRelatedTaskTicket());
			String related_answer = related_subtask.getAnswer();
			for (String item : items) { // category
				if ((!item.equals("N.A.")) && (!related_answer.contains(item))) {
					for (String val : Category.categories.get(item)) {

						// for each description of category, we will
						// have #(2) new subtasks.
						// they are each other's related subtask
						String discription = item + ";" + val;
						generateRelatedSubtasks(discription, TYPE.typein);
					}
				}
			}
		}
	}

}
