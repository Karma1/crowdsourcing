package com.visenze.vifind.config;

import com.visenze.vifind.util.PropertyHelper;

public class Config {

	// camfind api
	public static final String CAMFIND_CREATE_URL = PropertyHelper
			.getProperty("camfind.create");
	public static final String CAMFIND_STATUS_URL = PropertyHelper
			.getProperty("camfind.status");
	public static final String CAMFIND_API_KEY = PropertyHelper
			.getProperty("camfind.key");
	public static final String CAMFIND_LAT = "1.3000";
	public static final String CAMFIND_LON = "103.8000";
	public static final String CAMFIND_LANGUAGE = "en";
	public static final String CAMFIND_LOCALE = "en_SG";

	// visenze recognition api
	public final static String RECOGNITION_BASE_URL = PropertyHelper
			.getProperty("recognition.url");
	public final static String RECOGNITION_ACCESS_KEY = PropertyHelper
			.getProperty("recognition.accesskey");
	public final static String RECOGNITION_SECRET_KEY = PropertyHelper
			.getProperty("recognition.secretkey");
	public final static String RECOGNITION_PATH = "recognition";
	public final static String VITAG = "ViTag";
	public final static double CUT_OFF = 0.4;

	// visenze search api
	public final static String SEARCH_BASE_URL = PropertyHelper
			.getProperty("searchapi.url");
	public final static String SEARCH_ACCESS_KEY = PropertyHelper
			.getProperty("searchapi.accesskey");
	public final static String SEARCH_SECRET_KEY = PropertyHelper
			.getProperty("searchapi.secretkey");
	public final static String UPLOAD_SEARCH_PATH = "uploadsearch";

	// internal task system
	public static String TASK_BASE_URL = PropertyHelper
			.getProperty("taskserver.url");
	public static String TASK_CREATE_PATH = PropertyHelper
			.getProperty("taskserver.create");
	public static String TASK_STATUS_PATH = PropertyHelper
			.getProperty("taskserver.status");

	// method for recognition
	public static String V_API = "visenze_api";
	public static String C_API = "camfind";
	public static String V_TASK = "visenze_task";
	public static String V_COM = "visenze_combine";

	// others
	public final static String TEMP_PHOTO = "temp";
	public final static String LOG_FILE = "log.txt";

	public final static String highlevel = "/highlevel.txt";
}
