package com.visenze.vifind.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.visenze.vifind.config.Config;
import com.visenze.vifind.dto.DemoResponse;
import com.visenze.vifind.service.RecognitionService;

@Controller
public class RecognitionController {

	@Autowired
	private RecognitionService service;

	@RequestMapping(value = "/recognize", method = RequestMethod.POST)
	public @ResponseBody
	String handleFileUpload(@RequestParam("photo") MultipartFile file,
			@RequestParam("method") String method) {
		return service.recognize(file, method);
	}

	@RequestMapping(value = "/demo", method = RequestMethod.POST)
	public @ResponseBody
	DemoResponse handleFileUpload(@RequestParam("photo") MultipartFile file) {
		return service.demo(file);
	}

}
