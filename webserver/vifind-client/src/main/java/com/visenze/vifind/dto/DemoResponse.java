package com.visenze.vifind.dto;

/**
 * @author wgx731
 * 
 */
public class DemoResponse {

	private String status;
	private String searchJson;
	private String recognizeString;

	public DemoResponse(String status, String searchJson, String recognizeString) {
		super();
		this.status = status;
		this.searchJson = searchJson;
		this.recognizeString = recognizeString;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSearchJson() {
		return searchJson;
	}

	public void setSearchJson(String searchJson) {
		this.searchJson = searchJson;
	}

	public String getRecognizeString() {
		return recognizeString;
	}

	public void setRecognizeString(String recognizeString) {
		this.recognizeString = recognizeString;
	}

	@Override
	public String toString() {
		return "DemoResponse [status=" + status + ", searchJson=" + searchJson
				+ ", recognizeString=" + recognizeString + "]";
	}

}
