package com.visenze.vifind.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.visenze.vifind.config.Config;
import com.visenze.vifind.dto.DemoResponse;
import com.visenze.vifind.util.APIHelper;
import com.visenze.vifind.util.LogHelper;

@Service
public class RecognitionService {

	public String recognize(MultipartFile photo, String method) {
		String result = null;
		File f = null;
		if (!photo.isEmpty()) {
			try {
				// save file
				byte[] bytes = photo.getBytes();
				f = new File(Config.TEMP_PHOTO + ".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(f));
				stream.write(bytes);
				stream.close();
				String receivedTime = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(new Date());
				String methodType = "";
				long startTime = System.currentTimeMillis();
				// check method
				if (method.equalsIgnoreCase(Config.C_API)) {
					methodType = "camfind api";
					result = APIHelper.camFind(f);
				} else if (method.equalsIgnoreCase(Config.V_TASK)) {
					methodType = "visenze crowd sourcing system";
					result = APIHelper.internalTask(f);
				} else if (method.equalsIgnoreCase(Config.V_API)) {
					methodType = "visenze api";
					result = APIHelper.viRecognizeAPI(f);
				} else if (method.equalsIgnoreCase(Config.V_COM)) {
					methodType = "combine visenze api and crowd sourcing";
					result = APIHelper.viCombine(f);
				}

				// LOG DATA
				long finishTime = System.currentTimeMillis();
				String finishedTime = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(new Date());
				long duration = finishTime - startTime;
				LogHelper.log("job receive time: " + receivedTime
						+ "\njob finished time: " + finishedTime
						+ "\njob type: " + methodType + "\n job result: "
						+ result + "\ntotal time used: " + (duration / 1000F)
						+ " s\r\n======\r\n");
			} catch (Exception e) {
				result = "error";
			}
		} else {
			result = "error";
		}
		if (f != null) {
			f.delete();
		}
		return result;
	}

	public DemoResponse demo(MultipartFile photo) {
		DemoResponse response = null;
		File f = null;
		if (!photo.isEmpty()) {
			try {
				// save file
				byte[] bytes = photo.getBytes();
				f = new File(Config.TEMP_PHOTO + ".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(f));
				stream.write(bytes);
				stream.close();
				String receivedTime = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
				long startTime = System.currentTimeMillis();
				response = APIHelper.demo(f);
				// LOG DATA
				long finishTime = System.currentTimeMillis();
				String finishedTime = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
				long duration = finishTime - startTime;
				LogHelper.log("job receive time: " + receivedTime
						+ "\njob finished time: " + finishedTime
						+ "\njob result: " + response.getRecognizeString()
						+ "\ntotal time used: " + (duration / 1000F)
						+ " s\r\n======\r\n", "demolog.txt");
			} catch (Exception e) {
				e.printStackTrace();
				response = new DemoResponse("fail", "empty", "error: " + e);
			}
		} else {
			response = new DemoResponse("fail", "empty", "error: photo is empty");
		}
		if (f != null) {
			f.delete();
		}
		return response;
	}
}
