package com.visenze.vifind.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.visenze.vifind.config.Config;
import com.visenze.vifind.dto.DemoResponse;
import com.weardex.nlp.NlpService;
import com.weardex.nlp.NlpService.NlpObject;

/**
 * @author gaoxiang
 * 
 */
public class APIHelper {

	private static final Logger logger = LoggerFactory
			.getLogger(APIHelper.class);

	private static String encodeBase64(String source) {
		return new String(Base64.encodeBase64(source.getBytes()));
	}

	private static String getHeader(String accessKey, String secretKey) {
		StringBuilder sb = new StringBuilder();
		sb.append(accessKey);
		sb.append(":");
		sb.append(secretKey);
		String encoded = APIHelper.encodeBase64(sb.toString());
		sb = new StringBuilder();
		sb.append("Basic ");
		sb.append(encoded.substring(0, encoded.length() - 1));
		return sb.toString();
	}

	// Virecognition API
	public static String viRecognizeAPI(File f) {
		logger.info("visenze API method");
		long startTime = System.currentTimeMillis();
		logger.info("start time: " + startTime);
		String result = apiRecognize(f);
		long resultTime = System.currentTimeMillis();
		long duration = resultTime - startTime;
		logger.info("total Time: " + duration);
		return result;
	}

	private static String apiRecognize(File photo) {
		try {
			HttpResponse<JsonNode> request = Unirest
					.post(Config.RECOGNITION_BASE_URL + Config.RECOGNITION_PATH)
					.header("Authorization",
							getHeader(Config.RECOGNITION_ACCESS_KEY,
									Config.RECOGNITION_SECRET_KEY))
					.field("image", photo).field("tag_group", Config.VITAG)
					.asJson();
			logger.info("recognition response: " + request.getBody().toString());
			JSONArray tagGroups = request.getBody().getObject()
					.getJSONArray("result").getJSONObject(0)
					.getJSONArray("tag_groups");
			String result = "";
			if (tagGroups.length() > 0) {
				JSONArray tags = tagGroups.getJSONObject(0)
						.getJSONArray("tags");
				for (int i = 0; i < tags.length(); i++) {
					JSONObject tag = tags.getJSONObject(i);
					if (tag.getDouble("score") > Config.CUT_OFF
							&& (!getHighLevelList().contains(
									tag.getString("name")))) {
						String name = tag.getString("name");
						if (name.contains("|")) {
							result += name.substring(0, name.indexOf("|"))
									+ " ";
						} else {
							result += name + " ";
						}
					}

					logger.info(tag.getDouble("score")
							+ " "
							+ (getHighLevelList().contains(
									tag.getString("name")) ? "highleves"
									: "nothighlevel"));
				}
			} else {
				result = "";
			}
			return result;
		} catch (UnirestException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	// API ENd

	// Internal Worker API
	public static String internalTask(File f) {
		logger.info("internal worker method");
		long startTime = System.currentTimeMillis();
		logger.info("start time: " + startTime);
		String ticket = taskCreate(f);
		long createTime = System.currentTimeMillis();
		long duration = createTime - startTime;
		logger.info("create Time: " + duration);
		String result = taskStatus(ticket);
		long resultTime = System.currentTimeMillis();
		duration = resultTime - createTime;
		logger.info("result Time: " + duration);
		duration = resultTime - startTime;
		logger.info("total Time: " + duration);
		return result;
	}

	private static String taskCreate(File photo) {
		HttpResponse<JsonNode> request = null;
		try {
			request = Unirest
					.post(Config.TASK_BASE_URL + Config.TASK_CREATE_PATH)
					.field("photo", photo).asJson();
			return request.getBody().getObject().getString("ticket");
		} catch (UnirestException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	private static String taskStatus(String ticket) {
		try {
			HttpResponse<JsonNode> request = Unirest
					.get(Config.TASK_BASE_URL + Config.TASK_STATUS_PATH)
					.field("ticket", ticket).asJson();

			logger.info("result: " + request.getBody());
			String status = request.getBody().getObject().getString("status");
			while (!status.equals("completed")) {
				// pause for 1 second
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				request = Unirest
						.get(Config.TASK_BASE_URL + Config.TASK_STATUS_PATH)
						.field("ticket", ticket).asJson();
				status = request.getBody().getObject().getString("status");
				logger.info("status: " + status);
			}
			logger.info("result: " + request.getBody());
			return request.getBody().getObject().getString("result");
		} catch (UnirestException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	// TASK END

	// CamFind API
	public static String camFind(File f) {
		logger.info("camfind method");
		long startTime = System.currentTimeMillis();
		logger.info("start time: " + startTime);
		String token = camFindCreate(f);
		long createTime = System.currentTimeMillis();
		long duration = createTime - startTime;
		logger.info("create Time: " + duration);
		String result = camFindResult(token);
		long resultTime = System.currentTimeMillis();
		duration = resultTime - createTime;
		logger.info("result Time: " + duration);
		duration = resultTime - startTime;
		logger.info("total Time: " + duration);
		return result;
	}

	private static String camFindCreate(File photo) {
		HttpResponse<JsonNode> request = null;
		try {
			request = Unirest.post(Config.CAMFIND_CREATE_URL)
					.header("X-Mashape-Authorization", Config.CAMFIND_API_KEY)
					.field("image_request[locale]", Config.CAMFIND_LOCALE)
					.field("image_request[language]", Config.CAMFIND_LANGUAGE)
					.field("image_request[latitude]", Config.CAMFIND_LAT)
					.field("image_request[longitude]", Config.CAMFIND_LON)
					.field("image_request[image]", photo).asJson();
			logger.info(request.getBody().toString());
			return request.getBody().getObject().getString("token");
		} catch (UnirestException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	private static String camFindResult(String token) {
		try {
			HttpResponse<JsonNode> request = Unirest
					.get(Config.CAMFIND_STATUS_URL + token)
					.header("X-Mashape-Authorization", Config.CAMFIND_API_KEY)
					.asJson();

			String status = request.getBody().getObject().getString("status");
			while (!status.equals("completed")) {
				// pause for 1 second
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				request = Unirest
						.get(Config.CAMFIND_STATUS_URL + token)
						.header("X-Mashape-Authorization",
								Config.CAMFIND_API_KEY).asJson();
				status = request.getBody().getObject().getString("status");
				logger.info("status: " + status);
			}
			logger.info("result: " + request.getBody());
			String name = request.getBody().getObject().getString("name");
			return name;
		} catch (UnirestException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	// end camfind

	// combine visenze api and crowd sourcing
	public static String viCombine(File f) {
		String ticket = taskCreate(f);
		String result = viRecognizeAPI(f);

		if (result.equals("")) {
			result = taskStatus(ticket);
		}
		return result;
	}

	// end combine

	public static ArrayList<String> getHighLevelList() {
		ArrayList<String> result = new ArrayList<String>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(APIHelper.getBasePath()
					+ Config.highlevel));
			String line;
			while ((line = br.readLine()) != null) {
				result.add(line);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private static String getBasePath() {
		return APIHelper.class.getResource("/").getPath();
	}

	// demo
	public static DemoResponse demo(File photo) {
		DemoResponse returnValue = null;
		String searchResult = null;
		String recogResult = null;
		recogResult = getRecognitionString(photo);
		String[] results = recogResult.split("\\s+");
		String tag = null;
		for (String s : results) {
			if (isFashion(s)) {
				tag = s;
				break;
			}
		}
		if (tag == null) {
			searchResult = thirdPartySearch(photo, tag);
		} else {
			searchResult = visenzeSearch(photo, tag);
		}
		if (recogResult != null && searchResult != null) {
			returnValue = new DemoResponse("ok", searchResult, recogResult);
		} else {
			returnValue = new DemoResponse("fail", "empty", "empty");
		}
		return returnValue;
	}

	private static String thirdPartySearch(File photo, String tag) {
		// TODO: call third party
		return "empty";
	}

	private static String visenzeSearch(File photo, String tag) {
		HttpResponse<String> request;
		String result = null;
		try {
			request = Unirest
					.post(Config.SEARCH_BASE_URL + Config.UPLOAD_SEARCH_PATH)
					.header("Authorization",
							getHeader(Config.SEARCH_ACCESS_KEY,
									Config.SEARCH_SECRET_KEY))
					.field("image", photo).field("im_cate", tag)
					.field("node", "shoppe").field("fl", "_id,im_s3_url")
					.field("limit", "30").asString();
			logger.info(request.getBody().toString());
			result = request.getBody().toString();
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return result;
	}

	private static String getRecognitionString(File photo) {
		String ticket = taskCreate(photo);
		String result = viRecognizeAPI(photo);
		if (result.equals("")) {
			result = taskStatus(ticket);
		}
		return result;
	}

	private static boolean isFashion(String tag) {
		NlpService service = null;
		try {
			service = new NlpService();
		} catch (Exception e) {
			e.printStackTrace();
		}
		NlpObject res = service.parse(tag);
		if (res.getDetect().equals("others")) {
			return false;
		}
		return true;
	}
	// demo end

}
