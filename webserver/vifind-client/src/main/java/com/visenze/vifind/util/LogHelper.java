package com.visenze.vifind.util;

import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.visenze.vifind.config.Config;

/**
 * @author gaoxiang
 * 
 */
public class LogHelper {

	private static final Logger logger = LoggerFactory
			.getLogger(LogHelper.class);

	public static void log(String data) {
		try {
			FileWriter fw = new FileWriter(Config.LOG_FILE, true);
			fw.write(data);
			fw.close();
		} catch (IOException ioe) {
			logger.error("IOException: " + ioe.getMessage());
		}
	}

	public static void log(String data, String file) {
		try {
			FileWriter fw = new FileWriter(file, true);
			fw.write(data);
			fw.close();
		} catch (IOException ioe) {
			logger.error("IOException: " + ioe.getMessage());
		}
	}

}
