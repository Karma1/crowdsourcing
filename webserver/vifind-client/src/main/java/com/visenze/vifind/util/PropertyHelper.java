package com.visenze.vifind.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * @author gaoxiang
 * 
 */
public class PropertyHelper {

	private static Properties prop = null;

	private static Properties setUp() {
		prop = new Properties();
		try {
			prop.load(PropertyHelper.class
					.getResourceAsStream("/config.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	public static String getProperty(String key) {
		if (prop == null) {
			prop = setUp();
		}
		return prop.getProperty(key);
	}
}
