package com.visenze.vifind.config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class Category {

	public static HashMap<String, ArrayList<String>> categories = parse(Config.CATEGORY_FILE);
	public static String[] categoryList = (String[]) categories.keySet()
			.toArray(new String[0]);

	private static HashMap<String, ArrayList<String>> parse(String categoryFile) {
		HashMap<String, ArrayList<String>> ctgs = new HashMap<String, ArrayList<String>>();
		BufferedReader br;
		try {
			System.out.println(Category.class.getResource("/").getPath()
					+ categoryFile);
			br = new BufferedReader(new FileReader(Category.class.getResource(
					"/").getPath()
					+ categoryFile));
			String line = br.readLine();
			while (line != null) {
				String[] words = line.split("\\s+");
				ArrayList<String> discriptions = new ArrayList<String>();
				for (int i = 1; i < words.length; i++) {
					discriptions.add(words[i]);
				}
				ctgs.put(words[0], discriptions);
				line = br.readLine();
			}

			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ctgs;
	}

}
