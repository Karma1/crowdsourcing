package com.visenze.vifind.config;

import com.visenze.vifind.util.PropertyHelper;

public class Config {

	private static final String HOST = PropertyHelper.getProperty("web.host");
	public static final String SERVER_BASE = "http://" + HOST + ":8080/";
	public static final String SOCKET_SERVER_BASE = "http://" + HOST + ":9000/";
	public static final String IMAGE_FOLDER = "resources/images/";
	public static final String IMAGE_PATH = PropertyHelper
			.getProperty("web.context") + "/resources/images/";
	public static final String TASK_COLLECTION = "task";
	public static final String WORKER_COLLECTION = "worker";
	public static final String CATEGORY_FILE = "category";

}
