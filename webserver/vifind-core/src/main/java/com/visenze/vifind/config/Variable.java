package com.visenze.vifind.config;

public class Variable {
	public static int MARKTASK = 3;
	public static int TYPETASK = 2;
	public static int REVIEWTASK = 1;

//	public static String MARKQUESTION = "Please choose one or more words which describe the picture best.";
	public static String MARKQUESTION = "What does the picture's contain? (Choose all that matches)";
//	public static String REVIEWQUESTION = "Please choose one word which you think describe the picture best.";
	public static String REVIEWQUESTION = "Which is the $1's $2? (Or neither)";	
	public static String TYPEQUESTION = "What is the $1's $2?";

	public static String defaultStrategy = "default";
	public static String simpleStrategy = "simple";
	public static String onlyTypeinStrategy = "onlyTypein";
	public static String currentStrategy = simpleStrategy;
	
}
 