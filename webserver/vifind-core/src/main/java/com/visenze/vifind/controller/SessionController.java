package com.visenze.vifind.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.visenze.vifind.service.WorkerService;

@Controller
public class SessionController {

	private static final Logger logger = LoggerFactory
			.getLogger(SessionController.class);

	@Autowired
	private WorkerService workerService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String Login() {
		return "login";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register() {
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(HttpServletRequest request,
			@RequestParam(required = true) String username,
			@RequestParam(required = true) String password) {
		if (workerService.getWorkerByName(username) == null) {
			workerService.createWorker(username, password, 2);
			logger.info("new user " + username + " created");
			return "redirect:/login";
		} else {
			return "redirect:/register";
		}
	}
}
