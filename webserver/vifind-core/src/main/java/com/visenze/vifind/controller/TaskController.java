package com.visenze.vifind.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.visenze.vifind.config.Config;
import com.visenze.vifind.config.Variable;
import com.visenze.vifind.dto.BaseResponse;
import com.visenze.vifind.dto.CreateResponse;
import com.visenze.vifind.dto.GetResponse;
import com.visenze.vifind.dto.StatusResponse;
import com.visenze.vifind.model.STATUS;
import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;
import com.visenze.vifind.model.Worker;
import com.visenze.vifind.service.TaskService;
import com.visenze.vifind.service.WorkerService;
import com.visenze.vifind.strategy.Strategy;
import com.visenze.vifind.strategy.StrategyFactory;
import com.visenze.vifind.util.SocketIOHelper;

@Controller
public class TaskController {

	private static final Logger logger = LoggerFactory
			.getLogger(TaskController.class);

	@Autowired
	private TaskService taskService;
	@Autowired
	private WorkerService workerService;

	private Strategy strategy = StrategyFactory
			.useStrategy(Variable.currentStrategy);

	// create a parent task and then split into subtask
	@RequestMapping(value = "/tasks/create", method = RequestMethod.POST)
	public @ResponseBody
	CreateResponse createTask(HttpServletRequest request,
			@RequestParam("photo") MultipartFile file) {
		CreateResponse response;
		List<String> errors = new ArrayList<String>();
		if (!file.isEmpty()) {
			try {
				// write file
				byte[] bytes = file.getBytes();
				String filename = file.getOriginalFilename();
				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS")
						.format(new Date());
				ServletContext context = request.getSession()
						.getServletContext();
				filename = timeStamp + "_" + filename;
				File f = new File(context.getRealPath(Config.IMAGE_FOLDER
						+ filename));
				logger.info("File saved in: " + f.getAbsolutePath());
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(f));
				stream.write(bytes);
				stream.close();
				// create task
				Task t = new Task();
				t.setTicket(UUID.randomUUID().toString());
				t.setStatus(STATUS.waitting);
				t.setImageUrl(Config.SERVER_BASE + Config.IMAGE_PATH + filename);
				// t.splitTask();
				ArrayList<String> taskIDs = strategy.splitStrategy(t);
				taskService.createTask(t);
				// send signal
				logger.info(taskIDs + " send out");
				SocketIOHelper.send(taskIDs);
				response = new CreateResponse(t);
			} catch (IOException e) {
				logger.error(e.getMessage());
				errors.add("file is not supported.");
				response = new CreateResponse(errors);
			}
		} else {
			errors.add("empty file.");
			response = new CreateResponse(errors);
		}
		return response;
	}

	// check task status
	@RequestMapping(value = "/tasks/status", method = RequestMethod.GET)
	public @ResponseBody
	StatusResponse getTaskStatus(HttpServletRequest request,
			@RequestParam(required = true) String ticket) {
		StatusResponse r = new StatusResponse();
		List<String> errors = new ArrayList<String>();
		r.setMethod(BaseResponse.TASK_STATUS);
		Task t = taskService.getTaskByTicket(ticket);
		if (t == null) {
			errors.add("invalid parent ticket");
			r.setStatus(BaseResponse.FAIL);
			r.setError(errors);
		} else {
			r.setStatus(t.getStatus().toString());
			switch (t.getStatus()) {
			case completed:
				r.setResult(t.getTag());
				break;
			default:
				break;
			}
		}
		return r;
	}

	// display subtask to worker (ajax)
	@RequestMapping(value = "/tasks/display", method = RequestMethod.POST)
	public @ResponseBody
	GetResponse getTaskDetails(HttpServletRequest request,
			@RequestParam(required = true) String parentTicket,
			@RequestParam(required = true) String childTicket) {
		Task t = taskService.getTaskByTicket(parentTicket);
		GetResponse r = new GetResponse();
		r.setMethod(BaseResponse.TASK_GET);
		List<String> errors = new ArrayList<String>();
		if (t == null) {
			errors.add("invalid parent ticket");
			r.setStatus(BaseResponse.FAIL);
			r.setError(errors);
		} else {
			if (t.getWaitingTasks().containsKey(childTicket)) {
				Subtask st = t.getWaitingTasks().get(childTicket);
				r = new GetResponse(st.getType(), st.getDiscription());
				r.setStatus(BaseResponse.OK);
				r.setImageUrl(t.getImageUrl());
				r.setParentTicket(t.getTicket());
				r.setChildTicket(st.getTicket());
			} else {
				errors.add("invalid child ticket");
				r.setStatus(BaseResponse.FAIL);
				r.setError(errors);
			}
		}
		return r;
	}

	// submit subtask (ajax)
	@RequestMapping(value = "/tasks/submit", method = RequestMethod.POST)
	public synchronized @ResponseBody
	String submitTask(@RequestParam(required = true) String workerID,
			@RequestParam(required = true) String childTicket,
			@RequestParam(required = true) String parentTicket,
			@RequestParam(required = true) String type,
			@RequestParam(required = true) String startTime,
			@RequestParam(required = true) String ans) {
		TYPE taskType = null;
		if (type.equals("mark")) {
			taskType = TYPE.mark;
		} else if (type.equals("review")) {
			taskType = TYPE.review;
		} else if (type.equals("typein")) {
			taskType = TYPE.typein;
		}
		Task task = taskService.getTaskByTicket(parentTicket);
		Worker worker = workerService.getWorkerByID(workerID);
		ArrayList<String> newTasks = strategy.useStrategy(worker, childTicket,
				task, taskType, ans, startTime, new SimpleDateFormat(
						"yyyyMMdd_HHmmssSSS").format(new Date()));
		taskService.updateTask(task);
		workerService.updateWorker(worker);
		logger.info("task & worker updated");
		if (newTasks == null) {
			return "all task done.";
		} else {
			SocketIOHelper.send(newTasks);
			logger.info(newTasks + " send out");
			return "new subtask created.";
		}
	}
	// Used for Task update
	/*
	 * @RequestMapping(value = "/tasks/update", method = RequestMethod.POST)
	 * public @ResponseBody UpdateResponse updateTask(HttpServletRequest
	 * request,
	 * 
	 * @RequestParam(required = true) String ticket,
	 * 
	 * @RequestParam(required = false) String result) { Task t =
	 * taskService.getTaskByTicket(ticket); UpdateResponse r = new
	 * UpdateResponse(); if (t == null) { List<String> errors = new
	 * ArrayList<String>(); errors.add("invalid ticket");
	 * r.setStatus(BaseResponse.FAIL); r.setError(errors); } else { if (result
	 * != null) { t.setStatus(STATUS.completed); t.setTag(result);
	 * t.setFinishedTime(new SimpleDateFormat("yyyyMMdd_HHmmss") .format(new
	 * Date())); } else { t.setStatus(STATUS.working); }
	 * taskService.updateTask(t); r.setStatus(BaseResponse.OK); } return r; }
	 */

}
