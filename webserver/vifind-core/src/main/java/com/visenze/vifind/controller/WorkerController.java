package com.visenze.vifind.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visenze.vifind.dto.BaseResponse;
import com.visenze.vifind.dto.WorkerResponse;
import com.visenze.vifind.model.Worker;
import com.visenze.vifind.service.WorkerService;
import com.visenze.vifind.util.PropertyHelper;

@Controller
public class WorkerController {

	private static final Logger logger = LoggerFactory
			.getLogger(WorkerController.class);

	@Autowired
	private WorkerService workerService;

	@RequestMapping(value = "/workers", method = RequestMethod.GET)
	public String workerPannel(Model model) {
		model.addAttribute("host", PropertyHelper.getProperty("web.host"));
		model.addAttribute("context", PropertyHelper.getProperty("web.context"));
		return "workers";
	}

	@RequestMapping(value = "/workers/create", method = RequestMethod.GET)
	public @ResponseBody
	String createWorker() {
		logger.info("create admin and user");
		workerService.createWorker("admin", "admin", 1);
		workerService.createWorker("user", "user", 2);
		return "user created";
	}

	@RequestMapping(value = "/workers/status", method = RequestMethod.POST)
	public @ResponseBody
	WorkerResponse workerStatus(@RequestParam(required = true) String workerID) {
		Worker worker = workerService.getWorkerByID(workerID);
		List<String> errors = new ArrayList<String>();
		WorkerResponse r = new WorkerResponse();
		r.setMethod(BaseResponse.WORKER_STATUS);
		if (worker == null) {
			errors.add("invalid worker id");
			r.setStatus(BaseResponse.FAIL);
			r.setError(errors);
		} else {
			r.setStatus(BaseResponse.OK);
			r.setAnswerTime(worker.getAnswerTime());
			r.setResponseTime(worker.getResponseTime());
			r.setTotalCount(worker.getTotalCount());
			r.setValidCount(worker.getValidCount());
		}
		return r;
	}

}
