package com.visenze.vifind.dto;

import java.util.List;

/**
 * @author gaoxiang
 * 
 */
public class BaseResponse {

	public static final String OK = "ok";
	public static final String FAIL = "fail";
	public static final String TASK_CREATE = "task.create";
	public static final String TASK_STATUS = "task.status";
	public static final String TASK_GET = "task.get";
	public static final String WORKER_STATUS = "worker.status";

	private String status;
	private List<String> error;
	private String method;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getError() {
		return error;
	}

	public void setError(List<String> error) {
		this.error = error;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Override
	public String toString() {
		return "BaseResponse [status=" + status + ", error=" + error
				+ ", method=" + method + "]";
	}

}
