package com.visenze.vifind.dto;

import java.util.ArrayList;
import java.util.List;

import com.visenze.vifind.model.Task;

/**
 * @author gaoxiang
 * 
 */
public class CreateResponse extends BaseResponse {

	private String ticket;

	public CreateResponse(Task task) {
		this.ticket = task.getTicket();
		this.setError(new ArrayList<String>());
		this.setMethod(BaseResponse.TASK_CREATE);
		this.setStatus(BaseResponse.OK);
	}

	public CreateResponse(List<String> errors) {
		this.setError(errors);
		this.setMethod(BaseResponse.TASK_CREATE);
		this.setStatus(BaseResponse.FAIL);
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	@Override
	public String toString() {
		return "CreateResponse [ticket=" + ticket + ", Base="
				+ super.toString() + "]";
	}

}
