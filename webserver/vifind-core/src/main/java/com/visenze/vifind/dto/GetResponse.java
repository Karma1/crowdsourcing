package com.visenze.vifind.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.visenze.vifind.config.Variable;
import com.visenze.vifind.model.TYPE;

/**
 * @author gaoxiang
 * 
 */
public class GetResponse extends BaseResponse {

	private static final Logger logger = LoggerFactory
			.getLogger(GetResponse.class);

	private String childTicket;
	private String parentTicket;
	private TYPE type;
	private String imageUrl;
	private String question;
	private List<String> choices;

	public GetResponse() {

	}

	public GetResponse(TYPE type, String description) {
		this.type = type;
		String[] tempC = { };
		switch (type) {
		case mark:
			question = Variable.MARKQUESTION;
			choices = new ArrayList<String>();
			choices.addAll(Arrays.asList(description.split(";")));
			break;
		case review:
			question = Variable.REVIEWQUESTION;
			tempC = description.split(";");
			question = question.replace("$1", tempC[0]);
			question = question.replace("$2", tempC[1]);
			choices = new ArrayList<String>();
			choices.add(tempC[2]);
			choices.add(tempC[3]);
			break;
		case typein:
			String tempQ = Variable.TYPEQUESTION;
			tempC = description.split(";");
			tempQ = tempQ.replace("$1", tempC[0]);
			tempQ = tempQ.replace("$2", tempC[1]);
			question = tempQ;
			break;
		default:
			break;
		}
	}

	public String getChildTicket() {
		return childTicket;
	}

	public void setChildTicket(String childTicket) {
		this.childTicket = childTicket;
	}

	public String getParentTicket() {
		return parentTicket;
	}

	public void setParentTicket(String parentTicket) {
		this.parentTicket = parentTicket;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<String> getChoices() {
		return choices;
	}

	public void setChoices(List<String> choices) {
		this.choices = choices;
	}

	public TYPE getType() {
		return type;
	}

	public void setType(TYPE type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "GetResponse [childTicket=" + childTicket + ", parentTicket="
				+ parentTicket + ", type=" + type + ", imageUrl=" + imageUrl
				+ ", question=" + question + ", choices=" + choices + "]";
	}

}
