package com.visenze.vifind.dto;

/**
 * @author gaoxiang
 * 
 */
public class StatusResponse extends BaseResponse {

	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "StatusResponse [result=" + result + "]";
	}

}
