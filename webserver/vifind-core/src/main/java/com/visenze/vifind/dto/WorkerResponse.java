/**
 * 
 */
package com.visenze.vifind.dto;

/**
 * @author gaoxiang
 * 
 */
public class WorkerResponse extends BaseResponse {

	private int totalCount;
	private int validCount;
	private double responseTime;
	private double answerTime;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getValidCount() {
		return validCount;
	}

	public void setValidCount(int validCount) {
		this.validCount = validCount;
	}

	public double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}

	public double getAnswerTime() {
		return answerTime;
	}

	public void setAnswerTime(double answerTime) {
		this.answerTime = answerTime;
	}

	@Override
	public String toString() {
		return "WorkerResponse [totalCount=" + totalCount + ", validCount="
				+ validCount + ", responseTime=" + responseTime
				+ ", answerTime=" + answerTime + "]";
	}

}
