package com.visenze.vifind.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.visenze.vifind.config.Config;

@Document(collection = Config.TASK_COLLECTION)
public class Subtask {

	@Indexed
	@Id
	private String ticket;
	private String parentTicket;
	private String relatedTaskTicket;
	private String fatherSubtaskTicket;
	private TYPE type;
	private String discription;
	private String answer;
	private String imageUrl;
	private STATUS status;
	private String createTime;
	private String startTime;
	private String finishedTime;
	private String worker;
	
	public Subtask(){
		this.ticket = UUID.randomUUID().toString();
		this.parentTicket = "";
		this.relatedTaskTicket = "";
		this.fatherSubtaskTicket = "";
		this.type = TYPE.mark;
		this.discription = "";
		this.answer = "";
		this.imageUrl = "";
		this.status = STATUS.waitting;
		this.createTime = new SimpleDateFormat("yyyyMMdd_HHmmssSSS")
		.format(new Date());
		this.finishedTime = "";
		this.startTime = "";
		this.worker = "";
	}
	
	public Subtask(String parentTicket, String imageUrl) {
		this();
		this.parentTicket = parentTicket;
		this.imageUrl = imageUrl;
	}

	public Subtask(String parentTicket, String imageUrl,
			String discription) {
		this(parentTicket, imageUrl);
		this.discription = discription;
	}

	public Subtask(String parentTicket, String imageUrl,
			TYPE type) {
		this(parentTicket, imageUrl);
		this.type = type;
	}

	public Subtask(String parentTicket, String imageUrl,
			TYPE type, String discription) {
		this(parentTicket, imageUrl);
		this.type = type;
		this.discription = discription;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getParentTicket() {
		return parentTicket;
	}

	public void setParentTicket(String parentTicket) {
		this.parentTicket = parentTicket;
	}

	public TYPE getType() {
		return type;
	}

	public void setType(TYPE type) {
		this.type = type;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public String getFinishedTime() {
		return finishedTime;
	}

	public void setFinishedTime(String finishedTime) {
		this.finishedTime = finishedTime;
	}

	public String getWorker() {
		return worker;
	}

	public void setWorker(String worker) {
		this.worker = worker;
	}

	public String getRelatedTaskTicket() {
		return relatedTaskTicket;
	}

	public void setRelatedTaskTicket(String relatedTaskTicket) {
		this.relatedTaskTicket = relatedTaskTicket;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	@Override
	public String toString() {
		return "Subtask [ticket=" + ticket + ", parentTicket=" + parentTicket
				+ ", relatedTaskTicket=" + relatedTaskTicket + ", type=" + type
				+ ", discription=" + discription + ", answer=" + answer
				+ ", imageUrl=" + imageUrl + ", status=" + status
				+ ", createTime=" + createTime + ", startTime=" + startTime
				+ ", finishedTime=" + finishedTime + ", worker=" + worker + "]";
	}

	public String getFatherSubtaskTicket() {
		return fatherSubtaskTicket;
	}

	public void setFatherSubtaskTicket(String fatherSubtaskTicket) {
		this.fatherSubtaskTicket = fatherSubtaskTicket;
	}

}
