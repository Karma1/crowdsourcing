package com.visenze.vifind.model;

import java.util.HashMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.visenze.vifind.config.Config;

/**
 * @author gaoxiang
 * 
 *         task model
 */
@Document(collection = Config.TASK_COLLECTION)
public class Task {

	@Indexed
	@Id
	private String ticket;
	private String tag;
	private String imageUrl;
	private STATUS status;
	private String createTime;
	private String finishedTime;

	private HashMap<String, Subtask> waitingTasks;
	private HashMap<String, Subtask> finishedTasks;

	public Task() {
		this.ticket = "";
		this.tag = "";
		this.imageUrl = "";
		this.status = STATUS.working;
		this.createTime = "";
		this.finishedTime = "";
		this.waitingTasks = new HashMap<String, Subtask>();
		this.finishedTasks = new HashMap<String, Subtask>();
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getFinishedTime() {
		return finishedTime;
	}

	public void setFinishedTime(String finishedTime) {
		this.finishedTime = finishedTime;
	}

	@Override
	public String toString() {
		return "Task [ticket=" + ticket + ", tag=" + tag + ", imageUrl="
				+ imageUrl + ", status=" + status + "]";
	}

	public HashMap<String, Subtask> getWaitingTasks() {
		return waitingTasks;
	}

	public void setWaitingTasks(HashMap<String, Subtask> waitingTasks) {
		this.waitingTasks = waitingTasks;
	}

	public HashMap<String, Subtask> getFinishedTasks() {
		return finishedTasks;
	}

	public void setFinishedTasks(HashMap<String, Subtask> finishedTasks) {
		this.finishedTasks = finishedTasks;
	}

}
