package com.visenze.vifind.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.visenze.vifind.config.Config;

/**
 * @author gaoxiang
 * 
 *         worker model
 */
@Document(collection = Config.WORKER_COLLECTION)
public class Worker implements UserDetails {


	private static final long serialVersionUID = -1548007862150510055L;
	
	@Indexed
	@Id
	private String id;
	private String username;
	private String password;
	private int role;
	private int totalCount;
	private int validCount;
	private double responseTime;
	private double answerTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}

	public double getAnswerTime() {
		return answerTime;
	}

	public void setAnswerTime(double answerTime) {
		this.answerTime = answerTime;
	}

	@Override
	public String toString() {
		return "Worker [id=" + id + ", username=" + username + ", password="
				+ password + ", role=" + role + ", positiveCount="
				+ getTotalCount() + ", negativeCount=" + validCount
				+ ", responseTime=" + responseTime + ", answerTime="
				+ answerTime + "]";
	}

	private List<GrantedAuthority> getAuthorities(Integer role) {
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		if (role.intValue() == 1) {
			authList.add(new SimpleGrantedAuthority("ROLE_USER"));
			authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		} else if (role.intValue() == 2) {
			authList.add(new SimpleGrantedAuthority("ROLE_USER"));
		}
		return authList;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return getAuthorities(this.role);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Worker other = (Worker) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	public void update() {
		
	}

	public int getValidCount() {
		return validCount;
	}

	public void setValidCount(int validCount) {
		this.validCount = validCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
	public void incTotalCount() {
		totalCount ++;
	}
	
	public void incValidCount() {
		validCount ++;
	}
	
	public void decTotalCount() {
		totalCount --;
	}
	
	public void decValidCount() {
		validCount --;
	}
}
