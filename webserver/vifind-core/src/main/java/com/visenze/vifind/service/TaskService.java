package com.visenze.vifind.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.visenze.vifind.config.Config;
import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.Task;
import com.visenze.vifind.util.SocketIOHelper;

@Repository
public class TaskService {

	@PostConstruct
	public void sendOutWaitingTask() {
		SocketIOHelper.setup();
		List<Task> allTask = listTasks();
		if (allTask.size() > 0) {
			List<String> commands = new ArrayList<String>();
			for (Task t : allTask) {
				HashMap<String, Subtask> waiting = t.getWaitingTasks();
				if (waiting != null) {
					for (String childTicket : waiting.keySet()) {
						commands.add(getPriority(waiting.get(childTicket))
								+ ";" + t.getTicket() + ";" + childTicket);
					}
				}
			}
			SocketIOHelper.send(commands);
		}
	}

	private String getPriority(Subtask task) {
		switch (task.getType()) {
		case mark:
			return "0";
		case typein:
			return "1";
		case review:
			return "2";
		default:
			return "-1";
		}
	}

	@Autowired
	private MongoTemplate mongoTemplate;

	private void createCollection(String collectionName) {
		if (!mongoTemplate.collectionExists(collectionName)) {
			mongoTemplate.createCollection(collectionName);
		}
	}

	public void createTask(Task task) {
		createCollection(Config.TASK_COLLECTION);
		task.setCreateTime(new SimpleDateFormat("yyyyMMdd_HHmmssSSS")
				.format(new Date()));
		mongoTemplate.insert(task, Config.TASK_COLLECTION);
	}

	public List<Task> listTasks() {
		return mongoTemplate.findAll(Task.class, Config.TASK_COLLECTION);
	}

	public Task getTaskByTicket(String ticket) {
		Query q = new Query();
		q.addCriteria(Criteria.where("ticket").is(ticket));
		return mongoTemplate.findOne(q, Task.class, Config.TASK_COLLECTION);
	}

	public void removeTaskByTicket(String ticket) {
		mongoTemplate.remove(getTaskByTicket(ticket), Config.TASK_COLLECTION);
	}

	public void updateTask(Task task) {
		mongoTemplate.save(task, Config.TASK_COLLECTION);
	}

}