package com.visenze.vifind.service;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.visenze.vifind.config.Config;
import com.visenze.vifind.model.Worker;

@Repository
public class WorkerService implements UserDetailsService {

	private static final Logger logger = LoggerFactory
			.getLogger(WorkerService.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	private void createCollection(String collectionName) {
		if (!mongoTemplate.collectionExists(collectionName)) {
			mongoTemplate.createCollection(collectionName);
		}
	}

	public Worker getWorkerByName(String name) {
		Query q = new Query();
		q.addCriteria(Criteria.where("username").is(name));
		return mongoTemplate.findOne(q, Worker.class, Config.WORKER_COLLECTION);
	}

	public Worker getWorkerByID(String id) {
		Query q = new Query();
		q.addCriteria(Criteria.where("_id").is(id));
		return mongoTemplate.findOne(q, Worker.class, Config.WORKER_COLLECTION);
	}

	public void createWorker(String name, String password, int role) {
		createCollection(Config.WORKER_COLLECTION);
		Worker worker = new Worker();
		worker.setUsername(name);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		worker.setPassword(passwordEncoder.encode(password));
		worker.setId(UUID.randomUUID().toString());
		worker.setTotalCount(0);
		worker.setValidCount(0);
		worker.setRole(role);
		mongoTemplate.insert(worker, Config.WORKER_COLLECTION);
	}

	@Override
	public UserDetails loadUserByUsername(String name)
			throws UsernameNotFoundException {
		Worker user = getWorkerByName(name);
		if (user == null) {
			throw new UsernameNotFoundException(name + " not found.");
		}
		return user;
	}

	public void updateWorker(Worker worker) {
		mongoTemplate.save(worker, Config.WORKER_COLLECTION);
	}

}
