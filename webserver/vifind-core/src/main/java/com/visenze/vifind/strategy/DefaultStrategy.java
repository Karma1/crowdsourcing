package com.visenze.vifind.strategy;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;
import com.visenze.vifind.model.Worker;
import com.visenze.vifind.template.CheckboxTemplate;
import com.visenze.vifind.template.EmptyTemplate;
import com.visenze.vifind.template.ReviewTemplate;
import com.visenze.vifind.template.TemplateAbstract;
import com.visenze.vifind.template.TypeinTemplate;
import com.visenze.vifind.util.ApplicationContextHolder;

public class DefaultStrategy implements Strategy {

	@Override
	public ArrayList<String> useStrategy(Worker worker, String ticket,
			Task task, TYPE type, String answer, String startTime,
			String finishedTime) {
		TemplateAbstract template = null;
		if (type.equals(TYPE.mark)) {
			template = ApplicationContextHolder.getContext().getBean(
					TypeinTemplate.class);
		} else if (type.equals(TYPE.typein)) {
			template = ApplicationContextHolder.getContext().getBean(
					ReviewTemplate.class);
		} else if (type.equals(TYPE.review)) {
			template = ApplicationContextHolder.getContext().getBean(
					EmptyTemplate.class);
		}
		template.clearSubTaskList();
		template.processAnswer(worker, ticket, task, type, answer, startTime,
				finishedTime);
		template.generateSubtask();
		template.checkStatus();
		return template.getNewSubtasks();
	}

	@Override
	public ArrayList<String> splitStrategy(Task task) {
		TemplateAbstract templete = new CheckboxTemplate(task);
		templete.generateSubtask();
		return templete.getNewSubtasks();
	}

}
