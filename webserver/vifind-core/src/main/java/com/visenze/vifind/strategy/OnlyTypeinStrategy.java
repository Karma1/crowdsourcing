package com.visenze.vifind.strategy;

import java.util.ArrayList;

import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;
import com.visenze.vifind.model.Worker;
import com.visenze.vifind.template.OnlyTypeinResultTemplate;
import com.visenze.vifind.template.OnlyTypeinTemplate;
import com.visenze.vifind.template.TemplateAbstract;
import com.visenze.vifind.util.ApplicationContextHolder;

public class OnlyTypeinStrategy implements Strategy {

	@Override
	public ArrayList<String> splitStrategy(Task task) {
		// TODO Auto-generated method stub
		TemplateAbstract template = new OnlyTypeinTemplate(task);
		template.generateSubtask();
		return template.getNewSubtasks();
	}

	@Override
	public ArrayList<String> useStrategy(Worker worker, String ticket,
			Task task, TYPE type, String answer, String startTime,
			String finishedTime) {
		// TODO Auto-generated method stub
		TemplateAbstract template = ApplicationContextHolder.getContext().getBean(
				OnlyTypeinResultTemplate.class);
		template.clearSubTaskList();
		template.processAnswer(worker, ticket, task, type, answer, startTime,
				finishedTime);
		template.generateSubtask();
		template.checkStatus();
		return template.getNewSubtasks();
	}

}
