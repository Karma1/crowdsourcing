package com.visenze.vifind.strategy;

import java.util.ArrayList;

import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;
import com.visenze.vifind.model.Worker;
/**
 * The abstract for a strategy. A whole strategy contains splitStrategy and mainStrategy.
 * SplitStrategy is {@link splitStrategy}: used for splitting a new task to some small subtasks when it's created.
 * MainStratety is {@link useStrategy}: used for defining how to generate new subtasks when receiving an answer for a subtask.
 * @author chenc
 *
 */
public interface Strategy {
	
	/**
	 * Create a {@link com.visenze.vifind.template.TemplateAbstract} template, call {@link com.visenze.vifind.template.TemplateAbstract #generateSubtask}.
	 * @param task
	 * @return {@link com.visenze.vifind.template.TemplateAbstract #getNewSubtasks}
	 */
	public ArrayList<String> splitStrategy(Task task);
	
	/**
	 * Create a {@link com.visenze.vifind.template.TemplateAbstract} template, call 
	 * {@link com.visenze.vifind.template.TemplateAbstract #processAnswer},
	 * {@link com.visenze.vifind.template.TemplateAbstract #generateSubtask},
	 * {@link com.visenze.vifind.template.TemplateAbstract #checkStatus},
	 * and return {@link com.visenze.vifind.template.TemplateAbstract #getNewSubtasks}.
	 * @param worker
	 * @param ticket
	 * @param task
	 * @param type
	 * @param answer
	 * @param startTime
	 * @param finishedTime
	 * @return {@link com.visenze.vifind.template.TemplateAbstract #getNewSubtasks}
	 */
	public ArrayList<String> useStrategy(Worker worker, String ticket,
			Task task, TYPE type, String answer, String startTime,
			String finishedTime);
	
}
