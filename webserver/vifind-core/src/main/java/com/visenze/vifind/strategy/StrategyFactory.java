package com.visenze.vifind.strategy;

import com.visenze.vifind.config.Variable;
/**
 * Modify useStrategy to support your own strategy.
 * Add a new strategy name to {@link com.visenze.vifind.config.Variable}, and change currentStrategy to that strategy.
 * @author chenc
 *
 */
public class StrategyFactory {
	
	public static Strategy useStrategy(String strategyName) {
		if (strategyName.equals(Variable.defaultStrategy)) {
			return new DefaultStrategy();
		} else if (strategyName.equals(Variable.simpleStrategy)) {
			return new SimpleStrategy();
		} else if (strategyName.equals(Variable.onlyTypeinStrategy)) {
			return new OnlyTypeinStrategy();
		}
		else {
			return new DefaultStrategy();
		}
	}
}
