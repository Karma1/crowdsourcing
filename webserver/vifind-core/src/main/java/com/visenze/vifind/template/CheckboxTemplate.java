package com.visenze.vifind.template;

import org.springframework.stereotype.Component;

import com.visenze.vifind.config.Category;
import com.visenze.vifind.config.Variable;
import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;

@Component
public class CheckboxTemplate extends TemplateAbstract {

	public CheckboxTemplate() {
		super();
	}

	public CheckboxTemplate(Task t) {
		super(t);
	}

	@Override
	public void generateSubtask() {
		int item = 0;
		String discription = "";
		for (int i = 0; i < Category.categoryList.length; i++) {
			item++;
			discription += Category.categoryList[i] + ";";
			if (item % Variable.MARKTASK == 0) {
				discription += "N.A.";
				generateRelatedSubtasks(discription, TYPE.mark);
				discription = "";
			}
		}
		if (item % Variable.MARKTASK != 0) {
			discription += "N.A.";
			generateRelatedSubtasks(discription, TYPE.mark);
		}
	}
}
