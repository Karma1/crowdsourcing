package com.visenze.vifind.template;

import org.springframework.stereotype.Component;

@Component
public class OnlyTypeinResultTemplate extends TemplateAbstract {

	public OnlyTypeinResultTemplate() {
		super();
	}
	
	@Override
	public void generateSubtask() {
		// TODO Auto-generated method stub
		if (!discription.equals("N.A.")) {
			addTag(fatherSubtask.getAnswer());
		}
	}

}
