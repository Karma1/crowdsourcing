package com.visenze.vifind.template;

import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;

public class OnlyTypeinTemplate extends TemplateAbstract {
	
	private final int MAXNUM = 5;

	public OnlyTypeinTemplate() {
		super();
	}
	
	public OnlyTypeinTemplate(Task task) {
		// TODO Auto-generated constructor stub
		super(task);
	}

	@Override
	public void generateSubtask() {
		// TODO Auto-generated method stub
		for (int i = 0; i<5; i++) {
			String discription = "picture;key word";
			generateNewSubtask(discription, TYPE.typein);
		}
	}

}
