package com.visenze.vifind.template;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.TYPE;

@Component
public class ReviewTemplate extends TemplateAbstract {

	public ReviewTemplate() {
		super();
	}

	@Override
	public void generateSubtask() {
		// TODO Auto-generated method stub
		HashMap<String, Subtask> finishedTasks = task.getFinishedTasks();
		if (finishedTasks.containsKey(fatherSubtask.getRelatedTaskTicket())) {
			Subtask related_subtask = finishedTasks.get(fatherSubtask
					.getRelatedTaskTicket());
			// two related tasks are finished

			String stAnswer = fatherSubtask.getAnswer();
			String rsAnswer = related_subtask.getAnswer();
			if (stAnswer.equalsIgnoreCase(rsAnswer)) {
				// two answers are equal to each other
				// make it the final answer
				if (!stAnswer.equals("N.A.")) {
					addTag(stAnswer);	
				}

			} else {
				// not equal
				// create a review task
				Subtask review_subtask = new Subtask(task.getTicket(),
						task.getImageUrl(), TYPE.review,
						fatherSubtask.getDiscription() + ";" + stAnswer + ";"
								+ rsAnswer);
				review_subtask.setRelatedTaskTicket(fatherSubtask.getTicket());

				addSubTasks(review_subtask);

			}
		}
	}

}
