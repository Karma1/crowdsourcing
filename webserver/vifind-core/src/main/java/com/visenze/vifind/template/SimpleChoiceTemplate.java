package com.visenze.vifind.template;

import com.visenze.vifind.config.Category;
import com.visenze.vifind.config.Variable;
import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;

public class SimpleChoiceTemplate extends TemplateAbstract {

	public SimpleChoiceTemplate(Task task) {
		// TODO Auto-generated constructor stub
		super(task);
	}

	@Override
	public void generateSubtask() {
		// TODO Auto-generated method stub
		int item = 0;
		String description = "";
		for (int i = 0; i < Category.categoryList.length; i++) {
			item++;
			description += Category.categoryList[i] + ";";
			if (item % Variable.MARKTASK == 0) {
				description += "N.A.";
				Subtask subtask = new Subtask(task.getTicket(),
						task.getImageUrl(), TYPE.mark, description);
				addSubTasks(subtask);
				description = "";
			}
		}
		if (item % Variable.MARKTASK != 0) {
			description += "N.A.";
			Subtask subtask = new Subtask(task.getTicket(), task.getImageUrl(),
					TYPE.mark, description);
			addSubTasks(subtask);
		}
	}

}
