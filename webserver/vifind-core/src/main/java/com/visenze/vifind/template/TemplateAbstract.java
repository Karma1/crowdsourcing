package com.visenze.vifind.template;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.visenze.vifind.model.STATUS;
import com.visenze.vifind.model.Subtask;
import com.visenze.vifind.model.TYPE;
import com.visenze.vifind.model.Task;
import com.visenze.vifind.model.Worker;

@Component
public abstract class TemplateAbstract {

	private static final Logger logger = LoggerFactory
			.getLogger(TemplateAbstract.class);

	protected ArrayList<String> newSubtasks;
	protected Task task;
	protected Subtask fatherSubtask;
	protected String discription;

	public TemplateAbstract() {
		newSubtasks = new ArrayList<String>();
	}

	public TemplateAbstract(Task task) {
		newSubtasks = new ArrayList<String>();
		this.task = task;
	}

	public void clearSubTaskList() {
		newSubtasks = new ArrayList<String>();
	}

	abstract public void generateSubtask();

	public void processAnswer(Worker worker, String ticket, Task task,
			TYPE type, String answer, String startTime, String finishedTime) {
		// update subtask
		logger.info("wokerID " + worker.getId());
		logger.info("ticket " + ticket);
		logger.info("taskID " + task.getTicket());
		logger.info("answer " + answer);
		logger.info("finishTime " + finishedTime);
		HashMap<String, Subtask> waitingTasks = task.getWaitingTasks();
		HashMap<String, Subtask> finishedTasks = task.getFinishedTasks();
		Subtask subtask = waitingTasks.get(ticket);
		subtask.setWorker(worker.getId());
		subtask.setAnswer(answer);
		subtask.setStartTime(startTime);
		subtask.setFinishedTime(finishedTime);
		subtask.setStatus(STATUS.completed);

		// update worker
		try {
			double duration = (double) (new SimpleDateFormat(
					"yyyyMMdd_HHmmssSSS").parse(finishedTime).getTime() - new SimpleDateFormat(
					"yyyyMMdd_HHmmssSSS").parse(startTime).getTime());

			double avgDuration = (worker.getAnswerTime()
					* worker.getTotalCount() + duration)
					/ (double) (worker.getTotalCount() + 1);

			worker.setAnswerTime(avgDuration);
			worker.incTotalCount();
			worker.incValidCount();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// move subtask from waiting list to finished list
		finishedTasks.put(ticket, subtask);
		waitingTasks.remove(ticket);

		// update field parameter
		this.task = task;
		this.discription = answer;
		this.fatherSubtask = subtask;
	}

	public boolean checkStatus() {
		boolean isCompleted = task.getWaitingTasks().isEmpty();
		if (isCompleted) {
			task.setStatus(STATUS.completed);
			task.setFinishedTime(new SimpleDateFormat("yyyyMMdd_HHmmssSSS")
					.format(new Date()));
		}
		return isCompleted;
	}

	public ArrayList<String> getNewSubtasks() {
		if (newSubtasks.size() == 0)
			return null;
		return newSubtasks;
	}

	protected void generateRelatedSubtasks(String discription, TYPE type) {
		Subtask subtask1 = new Subtask(task.getTicket(), task.getImageUrl(),
				type, discription);
		Subtask subtask2 = new Subtask(task.getTicket(), task.getImageUrl(),
				type, discription);
		subtask1.setRelatedTaskTicket(subtask2.getTicket());
		subtask2.setRelatedTaskTicket(subtask1.getTicket());
		
		addSubTasks(subtask1);
		addSubTasks(subtask2);
	}
	
	protected void generateNewSubtask(String discription, TYPE type) {
		Subtask subtask = new Subtask(task.getTicket(), task.getImageUrl(),
				type, discription);
		addSubTasks(subtask);
	}

	protected void addSubTasks(Subtask subtask) {
		task.getWaitingTasks().put(subtask.getTicket(), subtask);
		newSubtasks.add(getPriority(subtask) + ";" + task.getTicket() + ";"
				+ subtask.getTicket());
	}

	protected String getPriority(Subtask task) {
		switch (task.getType()) {
		case mark:
			return "0";
		case typein:
			return "1";
		case review:
			return "2";
		default:
			return "-1";
		}
	}
	
	protected boolean addTag(String tag) {
		tag = tag.toLowerCase().trim();
		if (task.getTag().indexOf(tag) != -1) {
			return false;
		} else {
			String tags = task.getTag();
			if (tags.equals("")) {
				task.setTag(tag);
			} else {
				task.setTag(tags + " " + tag);
			}
			return true;
		}
	}

}
