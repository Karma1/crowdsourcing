package com.visenze.vifind.util;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;

import java.net.MalformedURLException;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.visenze.vifind.config.Config;

/**
 * @author gaoxiang
 * 
 */
public class SocketIOHelper {

	private static final Logger logger = LoggerFactory
			.getLogger(SocketIOHelper.class);

	private static SocketIO socket = null;

	public static void setup() {
		try {
			socket = new SocketIO(Config.SOCKET_SERVER_BASE + "input");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		socket.connect(new IOCallback() {

			@Override
			public void on(String arg0, IOAcknowledge arg1, Object... arg2) {

			}

			@Override
			public void onConnect() {
				logger.info("connected socket server");
			}

			@Override
			public void onDisconnect() {
				logger.info("disconnected socket server");
			}

			@Override
			public void onError(SocketIOException arg0) {
				logger.error("error connecting socket server");
			}

			@Override
			public void onMessage(String arg0, IOAcknowledge arg1) {
				logger.info("message received: " + arg0);
			}

			@Override
			public void onMessage(JSONObject arg0, IOAcknowledge arg1) {
				logger.info("message received: " + arg0);
			}

		});

	}

	public static void send(final List<String> IDs) {
		for (String id : IDs) {
			socket.send(id);
		}
	}
}
