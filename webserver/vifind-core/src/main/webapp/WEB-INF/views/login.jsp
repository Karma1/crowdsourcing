<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="worker demo">
<meta name="author" content="wgx731">

<title>Worker Demo Login Page</title>

<link href="resources/css/bootstrap.css" rel="stylesheet">
<link href="resources/css/login.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">

		<form class="form-signin" role="form" action="j_spring_security_check"
			method="POST">
			<h2 class="form-signin-heading">Please login in</h2>
			<c:if test="${not empty param.login_error}">
				<h5 class="red-text">
					<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}." />
				</h5>
			</c:if>
			<input type="text" class="form-control" placeholder="Name"
				required="true" autofocus="on" autocomplete="off" id="username"
				name="j_username" /> <input id="password" name="j_password"
				type="password" class="form-control" placeholder="Password"
				required="true" />
			<button class="btn btn-lg btn-primary btn-block" type="submit">Log
				In</button>
			<button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location='register';">Register
				</button>
		</form>
	</div>
</body>
</html>