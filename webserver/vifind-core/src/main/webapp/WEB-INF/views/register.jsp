<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="worker demo">
<meta name="author" content="wgx731">

<title>Worker Demo Register Page</title>

<link href="resources/css/bootstrap.css" rel="stylesheet">
<link href="resources/css/login.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">
		<form class="form-signin" role="form" action="register" method="POST">
			<h2 class="form-signin-heading">Please register.</h2>
			<input type="text" class="form-control" placeholder="Name"
				required="true" autofocus="on" autocomplete="off" id="username"
				name="username" /> <input id="password" name="password"
				type="password" class="form-control" placeholder="Password"
				required="true" />
			<button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
		</form>
	</div>
</body>
</html>