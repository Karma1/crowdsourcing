<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Worker Demo">
<meta name="author" content="wgx731">

<title>Worker Demo Main Page</title>

<link href="resources/css/bootstrap.css" rel="stylesheet">
<link href="resources/css/main.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">
		<div class="header">
			<ul class="nav nav-pills pull-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Worker Statics<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>Total Finished Task: <span id="w_count" class="badge">0</span></li>
						<li class="divider"></li>
						<li>Valid Finished Task: <span id="w_valid" class="badge">0</span></li>
						<li class="divider"></li>
						<li>Average Answer Time: <span id="w_answertime"
							class="badge">0</span></li>
						<li class="divider"></li>
					</ul></li>
				<li class="active"><a id="logout"
					href="<c:url value="/j_spring_security_logout"/>">Log out</a></li>
			</ul>
			<h3 class="text-muted">Worker Panel</h3>
		</div>

		<div class=hidden>
			<sec:authorize access="isAuthenticated()">
				<span id="username"><sec:authentication
						property="principal.username" /></span>
				<span id="role"><sec:authentication
						property="principal.authorities" /></span>
				<span id="workerID"><sec:authentication
						property="principal.id" /></span>
			</sec:authorize>
			<div id="console" class="panel-body"></div>
		</div>
		<div class="row marketing">
			<div id="worker-status" class="alert">Connecting ...</div>
			<div id="question-panel" class="panel panel-success"
				style="display: none;">
				<div class="panel-heading">
					<h3 class="panel-title">Question:</h3>
				</div>
				<div class="panel-body">
					<div id="picture"></div>
					<form id="q-form" class="form-inline">
						<h4 id="question"></h4>
						<div class="form-group">
							<div id="choices"></div>
						</div>
						<div class="form-group">
							<input id="input" type="text" />
						</div>
						<input type="hidden" id="parentTicket" /> <input type="hidden"
							id="childTicket" /> <input type="hidden" id="type" /> <input
							type="hidden" id="startTime" />
						<div class="form-group">
							<button id="result" type="button" onClick="submitTask();"
								class="btn btn-primary">Submit</button>
							<button id="report" type="button" onClick="reportError();"
								class="btn btn-danger">Don't Know</button>
						</div>
					</form>
				</div>
			</div>
			<center>
				<img id="loading-bar" src="resources/img/loading.gif" />
			</center>
		</div>

		<div class="footer">
			<p>&copy; Visenze 2013</p>
		</div>

	</div>
	<script src="resources/js/jquery-1.10.2.js"></script>
	<script src="resources/js/socket.io/socket.io.js"></script>
	<script src="resources/js/moment.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		var socketBase = 'http://${host}:9000/';
		var httpBase = 'http://${host}:8080/${context}/';
	</script>
	<script src="resources/js/main.js"></script>
</body>
</html>


