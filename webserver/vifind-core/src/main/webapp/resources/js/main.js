// get worker id
var workerID = null;
workerID = $('#workerID').text();
if (workerID === null) {
	alert('No worker. Something wrong!');
} else {
	console.log("worker id:" + workerID);
}

// setup socket
var socket = io.connect(socketBase + 'output');

// register worker
socket.on('connect', function() {
	socket.emit('registerWorker', {
		'message' : workerID
	});
	$('#loading-bar').show();
	changeWorkerStatus("alert alert-warning", "waiting for new job");
	log('<span>worker connected to server.</span>');
});

// remove worker
socket.on('disconnect', function() {
	socket.emit('logoutWorker', {
		'message' : workerID
	});
	$('#loading-bar').hide();
	changeWorkerStatus("alert alert-danger", "disconnected from server");
	log('<span>worker disconnected.</span>');
});

// display task
socket.on('message', function(data) {
	var tickets = data.split(";");
	fetchTask({
		parentTicket : tickets[1],
		childTicket : tickets[2]
	});
	$('#loading-bar').hide();
	changeWorkerStatus("alert alert-success", "doing job");
	log('<span> new task with ticket of <i class="red-text">' + tickets[2]
			+ '</i> received. </span>');
});

// get worker status
getWorkerStatics({
	workerID : workerID
});

function submitTask() {
	var answer = getAnswer($('#type').val());
	if (answer == '') {
		alert('answer cannot be null.');
	} else {
		var postData = {
			workerID : workerID,
			startTime : $('#startTime').val(),
			parentTicket : $('#parentTicket').val(),
			childTicket : $('#childTicket').val(),
			type : $('#type').val(),
			ans : answer
		};
		$('#input').val('');
		$('#question-panel').hide();
		console.log("====== QUESTION HIDE ======");
		postTask(postData);
		socket.emit("releaseWorker", {
			'message' : workerID
		});
		$('#loading-bar').show();
		changeWorkerStatus("alert alert-warning", "waiting for new job");
		log('<span>worker is free now.</span>');
	}

}

function reportError() {
	$('#input').val("N.A.");
	$('#result').click();
}

// Ajax Helper
function fetchTask(postData) {
	console.log("====== DISPLAY POST DATA: ======");
	console.log(postData);
	$.post(httpBase + "tasks/display", postData, function(data) {
		$('#startTime').val(moment().format('YYYYMMDD_HmmssSSS'));
		console.log("====== DISPLAY RESPONSE: ======");
		console.log(data);
		fillHidden(data.parentTicket, data.childTicket, data.type);
		addPic(data.imageUrl);
		addQuestion(data.question);
		$('#question-panel').show();
		if (data.type == 'mark') {
			addChoices(data.choices, 'checkbox');
			$('#choices').show();
			$('#input').hide();
			$('#report').hide();
		} else if (data.type == 'review') {
			addChoices(data.choices, 'radio');
			$('#choices').show();
			$('#input').hide();
			$('#report').hide();
		} else if (data.type == 'typein') {
			$('#choices').hide();
			$('#input').show();
			$(document).ready(function() {
				$('#input').focus();$('#input').val("");
				});
//			$('#input').focus();
//			$('#input').val("");
			$('#report').show();
		}
//		$('#question-panel').show();
		console.log("====== QUESTION SHOW ======");
	});
}

function postTask(postData) {
	console.log("====== SUBMIT TASK POST DATA: ======");
	console.log(postData);
	$.post(httpBase + "tasks/submit", postData, function(data) {
		console.log("====== SUBMIT TASK RESPONSE: ======");
		console.log(data);
		getWorkerStatics({
			workerID : workerID
		});
	});
}

function getWorkerStatics(postData) {
	console.log("====== WORKER STATUS POST DATA: ======");
	console.log(postData);
	$.post(httpBase + "workers/status", postData, function(data) {
		console.log("====== WORKER STATUS RESPONSE: ======");
		console.log(data);
		$('#w_count').text(data.totalCount);
		$('#w_valid').text(data.validCount);
		$('#w_answertime').text(data.answerTime + " ms");
	});
}

// UI Helper
function log(message) {
	$('#console').prepend($("<div>" + message + "</div>"));
}

function fillHidden(parentTicket, childTicket, type) {
	$('#parentTicket').val(parentTicket);
	$('#childTicket').val(childTicket);
	$('#type').val(type);
}

function getAnswer(type) {
	var answer = '';
	if (type != 'typein') {
		var choices = $('#choices').find('input');
		for (var i = 0, j = choices.length; i < j; i += 1) {
			if ($(choices[i]).is(':checked')) {
				answer += $(choices[i]).parent().text() + ";";
			}
		}
		answer = answer.substring(0, answer.length - 1);
	} else {
		answer = $('#input').val();
	}
	return answer;
}

function addPic(picUrl) {
	var pic = $('<img src="' + picUrl + '" width="' + 600 + '" height="' + 400
			+ '"></div>');
	var pictureHolder = $('#picture');
	pictureHolder.empty();
	pictureHolder.prepend(pic);
}

function addChoices(choices, type) {
	var choiceHolder = $('#choices');
	var choiceFront = '<label class="' + type + '-inline"><input type="' + type
			+ '" name="choices">';
	var choiceEnd = '</label>';
	var choiceHtml = '';
	var hasNA = false;
	choiceHolder.empty();
	for (var i = 0, j = choices.length; i < j; i += 1) {
		if (choices[i] == 'N.A.') {
			hasNA = true;
		}
		choiceHtml += choiceFront + choices[i] + choiceEnd;
	}
	if (type == 'radio' && !hasNA) {
		choiceHtml += choiceFront + 'N.A.' + choiceEnd;
	}
	choiceHolder.html(choiceHtml);
}

function addQuestion(question) {
	$('#question').text(question);
}

$('#q-form').submit(function(ev) {
	ev.preventDefault();
	submitTask();
});

function changeWorkerStatus(className, txt) {
	$('#worker-status').attr('class', className);
	$('#worker-status').text(txt);
}